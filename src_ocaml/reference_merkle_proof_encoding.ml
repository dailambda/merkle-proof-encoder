(*
   A reference implementation of Tezos Merkle proof encoding in simple OCaml

   The purpose of this code is to provide machine executable explanation
   of the encoding format without using complex techiniques for efficiency.

   WARNING: the correctness is NOT YET checked against the real code.
*)

module Bytes = struct
  include Bytes

  (* concatenation of a list of bytes *)
  let cats = Bytes.concat Bytes.empty

  (* return the len bytes prefix of bytes [bs] and the postfix.
     fails when the input is shorter than [len].

     Note that this function is slow and not for the real use.
  *)
  let split len bs =
    let blen = Bytes.length bs in
    if blen < len then failwith "Bytes.split: too short";
    (Bytes.sub bs 0 len, Bytes.sub bs len (blen - len))

  (* big endian, unsigned *)
  let to_int bytes =
    let acc = ref 0 in
    let len = Bytes.length bytes in
    Bytes.iteri (fun i c -> acc := !acc + Char.code c lsl ((len-i-1)*8)) bytes;
    !acc

  let () =
    assert (to_int (Bytes.of_string "\x01") = 0x01);
    assert (to_int (Bytes.of_string "\x01\x02") = 0x0102)

  (* big endian, unsigned

     note that for [bytes=8] the funciton only works properly
      within signed int63
  *)
  let of_int bytes i =
    assert (i >= 0);
    assert (bytes = 8 || i < (1 lsl (bytes * 8)));
    let buf = Bytes.create bytes in
    let i = ref i in
    for j = 0 to bytes - 1 do
      Bytes.unsafe_set buf (bytes - j - 1) (Char.chr (!i mod 256));
      i := !i / 256;
    done;
    buf

  let () =
    assert (to_int (Bytes.of_string "\x01") = 0x01);
    assert (to_int (Bytes.of_string "\x01\x02") = 0x0102);
    assert (of_int 1 0x01 = (Bytes.of_string "\x01"));
    assert (of_int 2 0x01 = (Bytes.of_string "\x00\x01"));
    assert (of_int 2 0x0102 = (Bytes.of_string "\x01\x02"));
    assert (of_int 3 0x0102 = (Bytes.of_string "\x00\x01\x02"))
end

(** uint8 *)

module Uint8 = struct
  let encode ui8 = Bytes.of_int 1 ui8

  let decode bytes =
    let bs, bytes = Bytes.split 1 bytes in
    Bytes.to_int bs, bytes

  let () =
    assert (encode 8 = Bytes.of_string "\x08");
    assert (decode (Bytes.of_string "\x08\x00") = (8, Bytes.of_string "\x00"))
end

(** uint16 *)

module Uint16 = struct
  let encode ui16 = Bytes.of_int 2 ui16

  let decode bytes =
    let bs, bytes = Bytes.split 2 bytes in
    Bytes.to_int bs, bytes

  let () =
    assert (encode 0x0813 = Bytes.of_string "\x08\x13");
    assert (decode (Bytes.of_string "\x08\x13\x00") = (0x0813, Bytes.of_string "\x00"))
end

(** uint32 *)

module Uint32 = struct
  let encode ui32 = Bytes.of_int 4 ui32

  let decode bytes =
    let bs, bytes = Bytes.split 4 bytes in
    Bytes.to_int bs, bytes

  let () =
    assert (encode 0x081349af = Bytes.of_string "\x08\x13\x49\xaf");
    assert (decode (Bytes.of_string "\x08\x13\x49\xaf\x00") = (0x081349af, Bytes.of_string "\x00"))
end

(** uint64... we do not care here that OCaml uses 63bit signed int *)

module Uint64 = struct
  let encode ui64 = Bytes.of_int 8 ui64

  let decode bytes =
    let bs, bytes = Bytes.split 8 bytes in
    Bytes.to_int bs, bytes

  let () =
    assert (encode 0x121349af00010203 = Bytes.of_string "\x12\x13\x49\xaf\x00\x01\x02\x03");
    assert (decode (Bytes.of_string "\x12\x13\x49\xaf\x00\x01\x02\x03\x00") = (0x121349af00010203, Bytes.of_string "\x00"))
end

(** Variable int for length *)

module Length = struct
  type length = int

  let encode length =
    let l =
      if length < 1 lsl 8 then 1
      else if length < 1 lsl 16 then 2
      else if length < 1 lsl 32 then 4
      else 8
    in
    let lbits =
      match l with
      | 1 -> 0b00
      | 2 -> 0b01
      | 4 -> 0b10
      | 8 -> 0b11
      | _ -> assert false
    in
    let length =
      match l with
      | 1 -> Uint8.encode length
      | 2 -> Uint16.encode length
      | 4 -> Uint32.encode length
      | 8 -> Uint64.encode length
      | _ -> assert false
    in
    (lbits, length)

  let decode lbits bytes =
    let l =
      match lbits with
      | 0b00 -> 1
      | 0b01 -> 2
      | 0b10 -> 4
      | 0b11 -> 8
      | _ -> assert false
    in
    let length, bytes =
      match l with
      | 1 -> Uint8.decode bytes
      | 2 -> Uint16.decode bytes
      | 4 -> Uint32.decode bytes
      | 8 -> Uint64.decode bytes
      | _ -> assert false
    in
    (length, bytes)

  let () =
    assert (encode 0x42 = (0b00, Bytes.of_string "\x42"));
    assert (decode 0b00 (Bytes.of_string "\x42\x00") = (0x42, Bytes.of_string "\x00"));
    assert (encode 0x0100 = (0b01, Bytes.of_string "\x01\x00"));
    assert (decode 0b01 (Bytes.of_string "\x01\x00\x00") = (0x0100, Bytes.of_string "\x00"));
    assert (encode 0x011234 = (0b10, Bytes.of_string "\x00\x01\x12\x34"));
    assert (decode 0b10 (Bytes.of_string "\x00\x01\x12\x34\x00") = (0x011234, Bytes.of_string "\x00"));
    assert (encode 0x123456789abcdef0 = (0b11, Bytes.of_string "\x12\x34\x56\x78\x9a\xbc\xde\xf0"));
    assert (decode 0b11 (Bytes.of_string "\x12\x34\x56\x78\x9a\xbc\xde\xf0\x00") = (0x123456789abcdef0, Bytes.of_string "\x00"))
end

(** Hash *)

module Hash = struct
  type hash = Hash of bytes (* 32 byte fixed *)

  let hash bytes =
    assert (Bytes.length bytes = 32);
    Hash bytes

  let encode (Hash bytes) = bytes

  let decode bytes =
    let bs, bytes = Bytes.split 32 bytes in
    (hash bs, bytes)

  let () =
    let bs = Bytes.of_string "\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0" in
    assert (encode (hash bs) = bs);
    assert (decode (Bytes.cat bs (Bytes.of_string "\x00")) = (hash bs, Bytes.of_string "\x00"))
end

(** Kinded hash *)

module Kinded_hash = struct
  type kinded_hash = Value of Hash.hash | Node of Hash.hash

  let encode kh =
    match kh with
    | Value hash ->
        Bytes.cats
          [ Uint8.encode 0x00;
            Hash.encode hash ]
    | Node hash ->
        Bytes.cats
          [ Uint8.encode 0x01;
            Hash.encode hash ]

  let decode bytes =
    let uint8, bytes = Uint8.decode bytes in
    match uint8 with
    | 0x00 ->
        let h, bytes = Hash.decode bytes in
        Value h, bytes
    | 0x01 ->
        let h, bytes = Hash.decode bytes in
        Node h, bytes
    | _ -> failwith "Invalid tag for Kinded_hash"

  let () =
    let bs = Bytes.of_string "\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0" in
    let h = Hash.hash bs in
    let postfix = Bytes.of_string "\x12\x34" in
    assert (decode (Bytes.cat (encode (Value h)) postfix) = (Value h, postfix));
    assert (decode (Bytes.cat (encode (Node h)) postfix) = (Node h, postfix))
end

(** Proof container *)

module Proof = struct
  type 'a proof = {
    version : int;
    before : Kinded_hash.kinded_hash;
    after : Kinded_hash.kinded_hash;
    state : 'a;
  }

  let encode state_encode proof =
    let y, before =
      match proof.before with
      | Value h -> 0x00, h
      | Node h -> 0x01, h
    in
    let z, after =
      match proof.after with
      | Value h -> 0x00, h
      | Node h -> 0x01, h
    in
    let tag = z lsl 1 + y in
    let tag = Uint8.encode tag in
    let version = Uint16.encode proof.version in
    let before = Hash.encode before in
    let after = Hash.encode after in
    let state = state_encode proof.state in
    let length = Uint32.encode (Bytes.length state) in
    Bytes.cats [tag; version; before; after; length; state]

  let decode state_decode bytes =
    let tag, bytes = Uint8.decode bytes in
    let version, bytes = Uint16.decode bytes in
    let before, bytes = Hash.decode bytes in
    let after, bytes = Hash.decode bytes in
    let length, bytes = Uint32.decode bytes in
    let state, bytes = state_decode length bytes in
    let z = (tag land 0b10) lsr 1 in
    let y = tag land 0b01 in
    let before =
      match y with
      | 0 -> Kinded_hash.Value before
      | 1 -> Kinded_hash.Node before
      | _ -> assert false
    in
    let after =
      match z with
      | 0 -> Kinded_hash.Value after
      | 1 -> Kinded_hash.Node after
      | _ -> assert false
    in
    { version; before; after; state }, bytes
end

(** The type for values. *)

type value = bytes

(** The type for file and directory names. *)

module Step = struct
  type step = string

  let encode s =
    assert (String.length s < 256);
    let length = Uint8.encode (String.length s) in
    Bytes.cat length (Bytes.of_string s)

  let decode bytes =
    let length, bytes = Uint8.decode bytes in
    let step, bytes = Bytes.split length bytes in
    Bytes.to_string step, bytes

  let () =
    assert (encode "hello" = Bytes.of_string "\x05hello");
    assert (decode (Bytes.of_string "\x05helloworld") = ("hello", Bytes.of_string "world"))
end

type index = int

module Inode = struct
  type 'a inode = {length : int; proofs : (index * 'a) list}
end

module Inode_extender = struct
  type 'a inode_extender = {length : int; segment : index list; proof : 'a}
end

(* concatenation of the element encodings *)
let list_encode element_encode xs =
  Bytes.cats (List.map element_encode xs)

(* the number of the elements are given *)
let list_decode_with_nelems element_decode length bytes =
  let rec f acc bytes n =
    if n = 0 then (List.rev acc ,bytes)
    else
      let element, bytes = element_decode bytes in
      f (element::acc) bytes (n-1)
  in
  f [] bytes length

(* the number of bytes of the element encodings is given *)
let list_decode_with_nbytes element_decode nbytes bytes =
  let bs, bytes = Bytes.split nbytes bytes in
  let rec f acc bytes =
    if Bytes.length bytes = 0 then List.rev acc
    else
      let element, bytes = element_decode bytes in
      f (element::acc) bytes
  in
  f [] bs, bytes

module Segment = struct

  (* Segment in the binary tree is a bit stream.  A terminal 10*
     is appended at the last byte to make the stream fit in a string.

     Note that if the number of the bits is a multiple of 8,
     the terminal is 10000000 which uses an entire byte.
  *)

  type segment = int list (* only 0 and 1 *)

  let encode segment =
    let length = (List.length segment + 8) / 8 in
    let bits = segment @ [1; 0; 0; 0; 0; 0; 0; 0] (* terminal *) in
    let rec f i bits =
      if i = length then []
      else
        match bits with
        | b1 :: b2 :: b3 :: b4 :: b5 :: b6 :: b7 :: b8 :: bits ->
            Uint8.encode (b1 * 128 + b2 * 64 + b3 * 32 + b4 * 16 + b5 * 8 + b6 * 4 + b7 * 2 + b8)
            :: f (i+1) bits
        | _ -> assert false
    in
    Bytes.cats (Uint8.encode length :: f 0 bits)

  let decode bytes =
    let length, bytes = Uint8.decode bytes in
    let bs, bytes = Bytes.split length bytes in
    let f i =
      let b = Char.code (Bytes.get bs i) in
      let bits =
        [ (if b land 128 = 0 then 0 else 1);
          (if b land 64 = 0 then 0 else 1);
          (if b land 32 = 0 then 0 else 1);
          (if b land 16 = 0 then 0 else 1);
          (if b land 8 = 0 then 0 else 1);
          (if b land 4 = 0 then 0 else 1);
          (if b land 2 = 0 then 0 else 1);
          (if b land 1 = 0 then 0 else 1) ]
      in
      if i < length - 1 then bits
      else
        (* the last byte. remove the terminal *)
        let rec remove_terminal = function
          | 1 :: bs -> List.rev bs
          | 0 :: bs -> remove_terminal bs
          | [] -> assert false
          | _ -> assert false
        in
        remove_terminal (List.rev bits)
    in
    List.concat (List.init length f), bytes

  let () =
    assert (encode [1;0;0;0;0;0;0] = Bytes.of_string "\001\129");
    assert (decode (Bytes.of_string "\001\129\x00") = ([1;0;0;0;0;0;0], Bytes.of_string "\x00"));
    assert (encode [1;0;0;0;0;0;0;1] = Bytes.of_string "\002\129\128");
    assert (decode (Bytes.of_string "\002\129\128\x00") = ([1;0;0;0;0;0;0;1], Bytes.of_string "\x00"));
    assert (decode (encode [0;1;0;1;0;1]) = ([0;1;0;1;0;1], Bytes.empty))
end

module Elt = struct
  type elt =
    | Value of value
    | Node of (Step.step * Kinded_hash.kinded_hash) list
    | Inode of Hash.hash Inode.inode
    | Inode_extender of Hash.hash Inode_extender.inode_extender

  let encode elt =
    match elt with
    | Value value ->
        let length = Bytes.length value in
        let lbits, length = Length.encode length in
        let tag = Uint8.encode (0b11000000 + lbits) in
        Bytes.cats [ tag; length; value ]
    | Node step_kinded_hash_list ->
        let length = List.length step_kinded_hash_list in
        assert (length <= 0b111111);
        let tag_length = Uint8.encode (0b10000000 + length) in
        let step_kinded_hash_list =
          list_encode
            (fun (step, kinded_hash) ->
               Bytes.cat (Step.encode step) (Kinded_hash.encode kinded_hash))
            step_kinded_hash_list
        in
        Bytes.cat tag_length step_kinded_hash_list
    | Inode hash_inode ->
        let lbits, length = Length.encode hash_inode.Inode.length in
        begin match hash_inode.Inode.proofs with
        | [(0,a); (1,b)] ->
            let w = 0b1 in
            let z = 0b1 in
            let tag = z lsl 3 + w lsl 2 + lbits in
            Bytes.cats [ Uint8.encode tag;
                         length;
                         Hash.encode a;
                         Hash.encode b ]
        | [(0,a)] ->
            let w = 0b1 in
            let z = 0b0 in
            let tag = z lsl 3 + w lsl 2 + lbits in
            Bytes.cats [ Uint8.encode tag;
                         length;
                         Hash.encode a ]
        | [(1,b)] ->
            let w = 0b0 in
            let z = 0b1 in
            let tag = z lsl 3 + w lsl 2 + lbits in
            Bytes.cats [ Uint8.encode tag;
                         length;
                         Hash.encode b ]
        | _ -> assert false
        end
    | Inode_extender hash_inode_extender ->
        let lbits, length = Length.encode hash_inode_extender.Inode_extender.length in
        let tag = Uint8.encode (0b11100000 + lbits) in
        let segment = Segment.encode hash_inode_extender.Inode_extender.segment in
        let hash = Hash.encode hash_inode_extender.Inode_extender.proof in
        Bytes.cats [ tag; length; segment; hash ]

  let decode bytes =
    let tag, bytes = Uint8.decode bytes in
    if tag lsr 7 = 0b0 then (* Inode *)
      let z = if tag land 0b00001000 = 0 then 0 else 1 in
      let w = if tag land 0b00000100 = 0 then 0 else 1 in
      let lbits = tag land 0b00000011 in
      let length, bytes = Length.decode lbits bytes in
      let hash0, bytes =
        if w = 0 then ([], bytes)
        else
          let hash, bytes = Hash.decode bytes in
          ([(0, hash)], bytes)
      in
      let hash1, bytes =
        if z = 0 then ([], bytes)
        else
          let hash, bytes = Hash.decode bytes in
          ([(1, hash)], bytes)
      in
      (Inode { length; proofs= hash0 @ hash1 }, bytes)
    else if tag lsr 6 = 0b10 then (* Node *)
      let length = tag land 0b0011111 in
      let step_kinded_hash_list, bytes =
        list_decode_with_nelems
          (fun bytes ->
             let step, bytes = Step.decode bytes in
             let kinded_hash, bytes = Kinded_hash.decode bytes in
             (step, kinded_hash), bytes)
          length bytes
      in
      (Node step_kinded_hash_list, bytes)
    else if tag lsr 5 = 0b110 then (* Value *)
      let lbits = tag land 0b00000011 in
      let length, bytes = Length.decode lbits bytes in
      let value, bytes = Bytes.split length bytes in
      Value value, bytes
    else if tag lsr 5 = 0b111 then (* Inode extender *)
      let lbits = tag land 0b00000011 in
      let length, bytes = Length.decode lbits bytes in
      let segment, bytes = Segment.decode bytes in
      let hash, bytes = Hash.decode bytes in
      Inode_extender { length; segment; proof= hash }, bytes
    else assert false

  let () =
    let test v =
      let postfix = Bytes.of_string "\x12\x34" in

      assert (let bs = encode v in decode (Bytes.cat bs postfix) = (v, postfix))
    in
    let h1 = Hash.hash (Bytes.of_string "\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xff") in
    let h2 = Hash.hash (Bytes.of_string "\xff\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde") in
    let h3 = Hash.hash (Bytes.of_string "\x99\x88\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc") in
    let h4 = Hash.hash (Bytes.of_string "\x00\xfe\xdc\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a") in
    test (Value (Bytes.of_string "hello"));
    test (Node [("step1", Value h1); ("stepstep2", Node h2)]);
    test (Inode { length = 999_000_000; proofs = [(0, h1); (1,h2)] });
    test (Inode { length = 999_000_999; proofs = [(0, h3)] });
    test (Inode { length = 123; proofs = [(1, h4)] });
    test (Inode_extender { length = 123_123_456_789; segment = [0; 1; 0; 1; 0; 1]; proof = h1 })
end

module Stream_proof = struct
  type stream_proof = Elt.elt list Proof.proof

  let encode = Proof.encode (list_encode Elt.encode)

  let decode = Proof.decode (list_decode_with_nbytes Elt.decode)

  let () =
    let test v =
      let postfix = Bytes.of_string "\x12\x34" in
      assert (let bs = encode v in decode (Bytes.cat bs postfix) = (v, postfix))
    in
    let h1 = Hash.hash (Bytes.of_string "\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xff") in
    let h2 = Hash.hash (Bytes.of_string "\xff\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde") in
    let h3 = Hash.hash (Bytes.of_string "\x99\x88\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc") in
    let h4 = Hash.hash (Bytes.of_string "\x00\xfe\xdc\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a\xbc\xde\xf0\x12\x34\x56\x78\x9a") in
    test { version = 130; before = Kinded_hash.Value h1; after = Kinded_hash.Node h2;
           state = [
             Elt.Value (Bytes.of_string "hello");
             Elt.Node [("step1", Value h1); ("stepstep2", Node h2)];
             Elt.Inode { length = 999_000_000; proofs = [(0, h1); (1,h2)] };
             Elt.Inode { length = 999_000_999; proofs = [(0, h3)] };
             Elt.Inode { length = 123; proofs = [(1, h4)] };
             Elt.Inode_extender { length = 123_123_456_789; segment = [0; 1; 0; 1; 0; 1]; proof = h1 }
           ] }

end
