# Merkle-Proof Encoder

Rust implementation of encoder for Tezos Merkle-proof

Each encoding versions are splited into 4 modules (v1_tree2, v1_tree32, v2_tree2, v2_tree32)
sharing structure definitions and trivial functions (proof_type.rs, common.rs).

Detailed specifications are in
- https://tezos.gitlab.io/developer/merkle-proof-encoding-formats.html
- https://hackmd.io/_bQ9Spe0QOmmqnJK2tjWCg

## Command-line
Can print the decoded proof in json style for given binary sequences

```
% echo "00000079fa51abae6eafcf5ede63c5a4ae4ed88c93f2d27dd1737fb92bef30ef308510902f9baa52fe7ef792b1a67eefad3ae528c3b17ca6bc211233dfa3ae26e1fb6c000000450e000a1ffd05dd40993dbc63b35ed99c03756e1b9f2fb98fb6500fb6e733b87c61f109538d8714a975edc317ac8f4aa7d15fca4c27b4ba1c26606cdf6b602c0f1a7031eba9" \
  | cargo run v2 tree2 stream
proof:
{"version":0,"before":{"Value":{"base64":"efpRq65ur89e3mPFpK5O2IyT8tJ90XN/uSvvMO8whRA="}},"after":{"Value":{"base64":"kC+bqlL+fveSsaZ+76065SjDsXymvCESM9+jribh+2w="}},"state":[{"Inode":{"length":663549,"proofs":[[0,{"base64":"Bd1AmT28Y7Ne2ZwDdW4bny+5j7ZQD7bnM7h8YfEJU40="}],[1,{"base64":"hxSpde3DF6yPSqfRX8pMJ7S6HCZgbN9rYCwPGnAx66k="}]]}}]}
bytes:
00000079fa51abae6eafcf5ede63c5a4ae4ed88c93f2d27dd1737fb92bef30ef308510902f9baa52fe7ef792b1a67eefad3ae528c3b17ca6bc211233dfa3ae26e1fb6c000000450e000a1ffd05dd40993dbc63b35ed99c03756e1b9f2fb98fb6500fb6e733b87c61f109538d8714a975edc317ac8f4aa7d15fca4c27b4ba1c26606cdf6b602c0f1a7031eba9
```
