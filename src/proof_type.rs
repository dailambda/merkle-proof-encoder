#[allow(unused_imports)]
use std::cmp;

type InBytes = [u8];
type RetBytes = Vec<u8>;
pub(crate) trait Encoding {
    fn of_bin(bin: &InBytes, off: &mut usize) -> Self;
    fn to_bin(self: Self) -> RetBytes;
}

use std::fmt::{self, Debug, Display, Formatter};

fn vec_fmt<T: Display>(src: &Vec<T>, f: &mut Formatter) -> fmt::Result {
    let r = write!(f, "[");
    let l = src.len();
    match src.last() {
        Some(le) => src[..l - 1]
            .iter()
            .fold(r, |r, e| r.and(write!(f, "{},", e)))
            .and(write!(f, "{}", le))
            .and(write!(f, "]")),
        None => r.and(write!(f, "[]")),
    }
}
fn vec2_fmt<T: Display, U: Display>(src: &Vec<(T, U)>, f: &mut Formatter) -> fmt::Result {
    let r = write!(f, "[");
    let l = src.len();
    match src.last() {
        Some((lf, ls)) => src[..l - 1]
            .iter()
            .fold(r, |r, (ef, es)| r.and(write!(f, "[{},{}],", ef, es)))
            .and(write!(f, "[{},{}]", lf, ls))
            .and(write!(f, "]")),
        None => r.and(write!(f, "[]")),
    }
}

#[derive(PartialEq, Eq, Clone)]
pub(crate) struct Bytes(Vec<u8>);

/* impl Bytes {
    pub(crate) fn new(bin: Vec<u8>) -> Bytes {
        Bytes(bin)
    }
} */

impl Debug for Bytes {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let x = &self.0;
        x.iter().fold(Ok(()), |p, x| p.and(write!(f, "{:02x}", x)))
    }
}

use base64::encode;
impl Display for Bytes {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let x = &self.0;
        let x = encode(x);
        write!(f, "{{\"base64\":\"{}\"}}", x)
    }
}

const HASH_LEN: usize = 32;

#[derive(Debug, PartialEq, Eq, Clone)]
struct Hash(Bytes);

impl Display for Hash {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let Hash(x) = self;
        write!(f, "{}", x)
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct Step(String);

impl Display for Step {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let Step(x) = self;
        write!(f, "\"{}\"", x.escape_debug().to_string())
    }
}

type Index = u8;
#[derive(Debug, PartialEq, Eq, Clone)]
struct Segment(Vec<Index>);

impl Display for Segment {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let Segment(x) = self;
        vec_fmt(x, f)
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct Seg<const N: usize>(Vec<Index>);

#[derive(Debug, PartialEq, Eq, Clone)]
struct Inode<T: Sized> {
    length: u64,
    proofs: Vec<(Index, T)>,
}

impl<T: Display> Display for Inode<T> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let Inode::<T> { length, proofs } = self;
        write!(f, "{{\"length\":{},\"proofs\":", length)
            .and(vec2_fmt(proofs, f))
            .and(write!(f, "}}"))
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct InodeExtender<T: Sized> {
    length: u64,
    segment: Segment,
    proof: T,
}

impl<T: Display> Display for InodeExtender<T> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let InodeExtender::<T> {
            length,
            segment,
            proof,
        } = self;
        write!(
            f,
            "{{\"length\":{},\"segment\":{},\"proof\":{}}}",
            length, segment, proof
        )
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
enum KindedHash {
    Value(Hash),
    Node(Hash),
}

impl Display for KindedHash {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        use KindedHash::*;
        let (tag, hash) = match self {
            Value(hash) => ("Value", hash),
            Node(hash) => ("Node", hash),
        };
        write!(f, "{{\"{}\":{}}}", tag, hash)
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
enum Elt {
    Value(Bytes),
    Node(Vec<(Step, KindedHash)>),
    Inode(Inode<Hash>),
    InodeExtender(InodeExtender<Hash>),
}

impl Display for Elt {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        use Elt::*;
        match self {
            Value(bytes) => write!(f, "{{\"Value\":{}}}", bytes),
            Node(trees) => write!(f, "{{\"Node\":")
                .and(vec2_fmt(trees, f))
                .and(write!(f, "}}")),
            Inode(inode) => write!(f, "{{\"Inode\":{}}}", inode),
            InodeExtender(ext) => write!(f, "{{\"Inode_extender\":{}}}", ext),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub(crate) struct StreamProof {
    version: u16,
    before: KindedHash,
    after: KindedHash,
    state: Vec<Elt>,
}

impl Display for StreamProof {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let StreamProof {
            version,
            before,
            after,
            state,
        } = self;
        let r = write!(
            f,
            "{{\"version\":{},\"before\":{},\"after\":{},\"state\":",
            version, before, after
        );
        r.and(vec_fmt(state, f)).and(write!(f, "}}"))
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
enum InodeTree {
    BlindedInode(Hash),
    InodeValues(Vec<(Step, Tree)>),
    InodeTrees(Box<Inode<InodeTree>>),
    InodeExtender(Box<InodeExtender<InodeTree>>),
}

impl Display for InodeTree {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        use InodeTree::*;
        match self {
            BlindedInode(hash) => write!(f, "{{\"Blinded_inode\":{}}}", hash),
            InodeValues(trees) => write!(f, "{{\"Inode_values\":")
                .and(vec2_fmt(trees, f))
                .and(write!(f, "}}")),
            InodeTrees(inode) => write!(f, "{{\"Inode_tree\":{}}}", inode),
            InodeExtender(ext) => write!(f, "{{\"Inode_extender\":{}}}", ext),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
enum Tree {
    Value(Bytes),
    BlindedValue(Hash),
    Node(Vec<(Step, Tree)>),
    BlindedNode(Hash),
    Inode(Inode<InodeTree>),
    Extender(InodeExtender<InodeTree>),
}

impl Display for Tree {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        use Tree::*;
        match self {
            Value(bytes) => write!(f, "{{\"Value\":{}}}", bytes),
            BlindedValue(hash) => write!(f, "{{\"Blinded_value\":{}}}", hash),
            Node(trees) => write!(f, "{{\"Node\":")
                .and(vec2_fmt(trees, f))
                .and(write!(f, "}}")),
            BlindedNode(hash) => write!(f, "{{\"Blinded_node\":{}}}", hash),
            Inode(inode) => write!(f, "{{\"Inode\":{}}}", inode),
            Extender(ext) => write!(f, "{{\"Extender\":{}}}", ext),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub(crate) struct TreeProof {
    version: u16,
    before: KindedHash,
    after: KindedHash,
    state: Tree,
}

impl Display for TreeProof {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let TreeProof {
            version,
            before,
            after,
            state,
        } = self;
        write!(
            f,
            "{{\"version\":{},\"before\":{},\"after\":{},\"state\":{}}}",
            version, before, after, state
        )
    }
}

include!("common.rs");
