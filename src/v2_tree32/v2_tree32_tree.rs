use crate::v2_tree32::*;

impl InodeTree {
    fn blinded_inode_of_bin(bin: &[u8], off: &mut usize) -> InodeTree {
        *off += 1;
        let hash = Hash::of_bin(bin, off);
        InodeTree::BlindedInode(hash)
    }
    fn blinded_inode_to_bin(src: Hash) -> RetBytes {
        //0b11000000
        let tag = 0b11000000u8;
        let mut v = vec![tag];
        v.append(&mut src.to_bin());
        v
    }

    fn inode_values_of_bin(bin: &[u8], off: &mut usize) -> InodeTree {
        let tag = u8::of_bin(bin, off);
        let length = (tag & 0b00111111u8) as usize;
        let mut v = vec![];
        for _i in 0..length {
            let step = Step::of_bin(bin, off);
            let tree = Tree::of_bin(bin, off);
            v.push((step, tree))
        }
        InodeTree::InodeValues(v)
    }
    fn inode_values_to_bin(src: Vec<(Step, Tree)>) -> RetBytes {
        let mut v = vec![];
        //length <= 32
        let length = cmp::min(src.len() as u8, 63u8);
        //0b10yyyyyy
        let tag = 0b10000000u8 | length;
        v.push(tag);
        if length >= 63 {
            v.append(&mut (src.len() as u32).to_bin())
        }
        for (s, t) in src.into_iter() {
            v.append(&mut s.to_bin());
            v.append(&mut t.to_bin())
        }
        v
    }

    fn inode_trees_of_bin(bin: &[u8], off: &mut usize) -> InodeTree {
        let tag = u8::of_bin(bin, off);
        let tag_for_len = tag & 0b00000011u8;
        let p = 1 << tag_for_len;
        let length = usize_of_bin(bin, off, p) as u64;

        let sd_tag = tag & (1 << 6) > 0;
        let mut v = vec![];
        if sd_tag {
            //Dense case
            for i in 0..32 {
                let it = Option::<InodeTree>::of_bin(bin, off);
                it.map(|it| v.push((i, it)));
            }
        } else {
            //Sparse case
            let it_len = (tag & 0b00111100u8) >> 2;

            for _i in 0..it_len {
                let j = u8::of_bin(bin, off);
                let it = Option::<InodeTree>::of_bin(bin, off).unwrap();
                v.push((j, it));
            }
        }
        let inode_trees = Box::<Inode<InodeTree>>::new(Inode::<InodeTree> { length, proofs: v });
        InodeTree::InodeTrees(inode_trees)
    }
    fn inode_trees_to_bin(Inode { length, proofs }: Inode<InodeTree>) -> RetBytes {
        let mut v = vec![];

        let proofs_len = proofs.len() as u8;
        if proofs_len >= 15 {
            //Dense Case
            //0b010000zz
            let (mut length, ltag) = usize_to_bin(length as usize);
            let tag = 0b01000000u8 | ltag;
            v.push(tag);
            v.append(&mut length);

            let mut ary = vec![None; 32];
            for (i, it) in proofs.into_iter() {
                ary[i as usize] = Some(it)
            }
            for oit in ary.into_iter() {
                v.append(&mut oit.to_bin());
            }
            v
        } else {
            //Sparse Case
            //0b00yyyyzz
            let (mut length, ltag) = usize_to_bin(length as usize);
            let tag = (proofs_len << 2) | ltag;
            v.push(tag);
            v.append(&mut length);

            for (i, it) in proofs.into_iter() {
                v.push(i);
                let it = Some(it);
                v.append(&mut it.to_bin());
            }
            v
        }
    }

    fn inode_extender_of_bin(bin: &[u8], off: &mut usize) -> InodeTree {
        let tag = u8::of_bin(bin, off);
        let tag_for_len = tag & 0b00000011u8;
        let p = 1 << tag_for_len;
        let length = usize_of_bin(bin, off, p) as u64;

        let segment = Segment::of_bin(bin, off);
        let proof = InodeTree::of_bin(bin, off);
        let inode_extender = Box::<InodeExtender<InodeTree>>::new(InodeExtender::<InodeTree> {
            length,
            segment,
            proof,
        });

        InodeTree::InodeExtender(inode_extender)
    }
    fn inode_extender_to_bin(
        InodeExtender {
            length,
            segment,
            proof,
        }: InodeExtender<InodeTree>,
    ) -> RetBytes {
        let mut v = vec![];
        //0b110100yy
        let tag = 0b11010000u8;

        let (mut length, ltag) = usize_to_bin(length as usize);
        v.push(tag | ltag);
        v.append(&mut length);
        v.append(&mut segment.to_bin());
        v.append(&mut proof.to_bin());
        v
    }
}

impl Encoding for InodeTree {
    fn of_bin(bin: &InBytes, off: &mut usize) -> InodeTree {
        let tag = bin[*off];
        let tree = if tag & (1 << 7) > 0 {
            if tag & (1 << 6) > 0 {
                match (tag >> 4) - 12 {
                    //0b00yy000
                    0 => InodeTree::blinded_inode_of_bin(bin, off),
                    1 => InodeTree::inode_extender_of_bin(bin, off),
                    _ => panic!(),
                }
            } else {
                //InodeValues
                InodeTree::inode_values_of_bin(bin, off)
            }
        } else {
            //InodeTrees
            InodeTree::inode_trees_of_bin(bin, off)
        };
        tree
    }
    fn to_bin(self) -> RetBytes {
        use InodeTree::*;
        match self {
            BlindedInode(hash) => InodeTree::blinded_inode_to_bin(hash),
            InodeValues(subtree) => InodeTree::inode_values_to_bin(subtree),
            InodeTrees(inode_proofs) => InodeTree::inode_trees_to_bin(*inode_proofs),
            InodeExtender(ext) => InodeTree::inode_extender_to_bin(*ext),
        }
    }
}

impl Encoding for Option<InodeTree> {
    fn of_bin(bin: &InBytes, off: &mut usize) -> Option<InodeTree> {
        let tag = bin[*off];
        if tag >> 4 == 0b1110u8 {
            *off += 1;
            None
        } else {
            Some(InodeTree::of_bin(bin, off))
        }
    }
    fn to_bin(self) -> RetBytes {
        match self {
            None => {
                //0b11100000
                let tag = 0b11100000;
                vec![tag]
            }
            Some(it) => it.to_bin(),
        }
    }
}

impl Tree {
    fn value_of_bin(bin: &InBytes, off: &mut usize) -> Tree {
        let tag = u8::of_bin(bin, off);
        let tag = tag & 0b00000011u8;
        let p = if tag == 0 {
            1
        } else if tag == 1 {
            2
        } else {
            4
        };
        let length = usize_of_bin(bin, off, p);
        let content = bytes_range(bin, off, length).to_vec();
        Tree::Value(Bytes(content))
    }

    fn value_to_bin(src: Bytes) -> RetBytes {
        let mut v = vec![];
        let Bytes(value) = src;
        let length = value.len();
        //0b110000yy
        let tag = 0b11000000u8;
        if length < 1 << 8 {
            v.push(tag);
            v.push(length as u8)
        } else if length < (1 << 16) {
            let tag = tag | 0b01;
            v.push(tag);
            v.append(&mut (length as u16).to_bin())
        } else {
            let tag = tag | 0b11;
            v.push(tag);
            v.append(&mut (length as u32).to_bin())
        }
        v.extend(value);
        v
    }

    fn blinded_value_of_bin(bin: &[u8], off: &mut usize) -> Tree {
        *off += 1;
        let hash = Hash::of_bin(bin, off);
        Tree::BlindedValue(hash)
    }
    fn blinded_value_to_bin(src: Hash) -> RetBytes {
        let tag = 0b11001000u8;
        let mut v = vec![tag];
        v.append(&mut src.to_bin());
        v
    }

    fn node_of_bin(bin: &[u8], off: &mut usize) -> Tree {
        let tag = u8::of_bin(bin, off);
        let length = (tag & 0b00111111u8) as usize;
        let mut v = vec![];
        for _i in 0..length {
            let step = Step::of_bin(bin, off);
            let tree = Tree::of_bin(bin, off);
            v.push((step, tree))
        }
        Tree::Node(v)
    }
    fn node_to_bin(src: Vec<(Step, Tree)>) -> RetBytes {
        let mut v = vec![];
        //length <= 32
        let length = cmp::min(src.len() as u8, 63u8);
        //0b10yyyyyy
        let tag = 0b10000000u8 | length;
        v.push(tag);
        if length >= 63 {
            v.append(&mut (src.len() as u32).to_bin())
        }
        for (s, t) in src.into_iter() {
            v.append(&mut s.to_bin());
            v.append(&mut t.to_bin())
        }
        v
    }

    fn blinded_node_of_bin(bin: &[u8], off: &mut usize) -> Tree {
        *off += 1;
        let hash = Hash::of_bin(bin, off);
        Tree::BlindedNode(hash)
    }
    fn blinded_node_to_bin(src: Hash) -> RetBytes {
        let tag = 0b11010000u8;
        let mut v = vec![tag];
        v.append(&mut src.to_bin());
        v
    }

    fn inode_of_bin(bin: &[u8], off: &mut usize) -> Tree {
        let tag = u8::of_bin(bin, off);
        let tag_for_len = tag & 0b00000011u8;
        let p = 1 << tag_for_len;
        let length = usize_of_bin(bin, off, p) as u64;

        let sd_tag = tag & (1 << 6) > 0;
        let mut v = vec![];
        if sd_tag {
            //Dense case
            for i in 0..32 {
                let it = Option::<InodeTree>::of_bin(bin, off);
                it.map(|it| v.push((i, it)));
            }
        } else {
            //Sparse case
            let hash_len = (tag & 0b00111100u8) >> 2;

            for _i in 0..hash_len {
                let j = u8::of_bin(bin, off);
                let it = Option::<InodeTree>::of_bin(bin, off).unwrap();
                v.push((j, it));
            }
        }
        Tree::Inode(Inode::<InodeTree> { length, proofs: v })
    }
    fn inode_to_bin(Inode { length, proofs }: Inode<InodeTree>) -> RetBytes {
        let mut v = vec![];

        let proofs_len = proofs.len() as u8;
        if proofs_len >= 15 {
            //Dense Case
            //0b010000zz
            let (mut length, ltag) = usize_to_bin(length as usize);
            let tag = 0b01000000u8 | ltag;
            v.push(tag);
            v.append(&mut length);

            let mut ary = vec![None; 32];
            for (i, it) in proofs.into_iter() {
                ary[i as usize] = Some(it)
            }
            for oit in ary.into_iter() {
                v.append(&mut oit.to_bin());
            }
            v
        } else {
            //Sparse Case
            //0b00yyyyzz
            let (mut length, ltag) = usize_to_bin(length as usize);
            let tag = (proofs_len << 2) | ltag;
            v.push(tag);
            v.append(&mut length);

            for (i, it) in proofs.into_iter() {
                v.push(i);
                let it = Some(it);
                v.append(&mut it.to_bin());
            }
            v
        }
    }

    fn extender_of_bin(bin: &[u8], off: &mut usize) -> Tree {
        let tag = u8::of_bin(bin, off);
        let tag_for_len = tag & 0b00000011u8;
        let p = 1 << tag_for_len;
        let length = usize_of_bin(bin, off, p) as u64;

        let segment = Segment::of_bin(bin, off);
        let proof = InodeTree::of_bin(bin, off);

        Tree::Extender(InodeExtender::<InodeTree> {
            length,
            segment,
            proof,
        })
    }
    fn extender_to_bin(
        InodeExtender {
            length,
            segment,
            proof,
        }: InodeExtender<InodeTree>,
    ) -> RetBytes {
        let mut v = vec![];
        //0b110110yy
        let tag = 0b11011000u8;

        let (mut length, ltag) = usize_to_bin(length as usize);
        v.push(tag | ltag);
        v.append(&mut length);
        v.append(&mut segment.to_bin());
        v.append(&mut proof.to_bin());
        v
    }
}

impl Encoding for Tree {
    fn of_bin(bin: &InBytes, off: &mut usize) -> Tree {
        let tag = bin[*off];
        let tree = if tag & (1 << 7) > 0 {
            if tag & (1 << 6) > 0 {
                match (tag >> 3) - 24 {
                    //0b11yyy00
                    0 => Tree::value_of_bin(bin, off),
                    1 => Tree::blinded_value_of_bin(bin, off),
                    2 => Tree::blinded_node_of_bin(bin, off),
                    3 => Tree::extender_of_bin(bin, off),
                    _ => panic!(),
                }
            } else {
                //Node
                Tree::node_of_bin(bin, off)
            }
        } else {
            //Inode
            Tree::inode_of_bin(bin, off)
        };
        tree
    }
    fn to_bin(self) -> RetBytes {
        use Tree::*;
        match self {
            Value(v) => Tree::value_to_bin(v),
            BlindedValue(hash) => Tree::blinded_value_to_bin(hash),
            Node(subtree) => Tree::node_to_bin(subtree),
            BlindedNode(hash) => Tree::blinded_node_to_bin(hash),
            Inode(inode_proofs) => Tree::inode_to_bin(inode_proofs),
            Extender(ext) => Tree::extender_to_bin(ext),
        }
    }
}

impl Encoding for TreeProof {
    fn of_bin(bin: &InBytes, off: &mut usize) -> TreeProof {
        let tag = u8::of_bin(bin, off);
        let version = u16::of_bin(bin, off);

        use KindedHash::{Node, Value};
        let before = {
            let hash = Hash::of_bin(bin, off);
            if tag & 0b01u8 > 0 {
                Node(hash)
            } else {
                Value(hash)
            }
        };
        let after = {
            let hash = Hash::of_bin(bin, off);
            if tag & 0b10u8 > 0 {
                Node(hash)
            } else {
                Value(hash)
            }
        };

        let state = Tree::of_bin(&bin, off);
        TreeProof {
            version,
            before,
            after,
            state,
        }
    }
    fn to_bin(self) -> RetBytes {
        let TreeProof {
            version,
            before,
            after,
            state,
        } = self;
        let tag = before.get_tag() | (after.get_tag() << 1);
        let mut version = version.to_bin();
        let mut before = before.get_hash().unwrap();
        let mut after = after.get_hash().unwrap();
        let mut state = state.to_bin();
        let mut v = vec![tag];
        v.append(&mut version);
        v.append(&mut before);
        v.append(&mut after);
        v.append(&mut state);
        v
    }
}
