mod v2_tree2_stream;
mod v2_tree2_tree;

include!("proof_type.rs");

type S = Seg<1>;
impl Encoding for Segment {
    fn of_bin(bin: &InBytes, off: &mut usize) -> Segment {
        Segment(S::of_bin(bin, off).0)
    }
    fn to_bin(self) -> RetBytes {
        let Segment(src) = self;
        S::to_bin(Seg(src))
    }
}

#[test]
fn v2_tree2_stream_test() {
    let input = "0200034f645d70ce58a88c07d2a63b70d56b7580fa5c2bdf59e441346b8fd3a3b3904368ce49a257342123fe87d17d2c5018e5b0fda3e4a89984cdcf4a630734e1dd3e0000017582062966387c3e2d012b21c322ff1865e76e0cf101556816d68bd94bb235214b9398b895b5fb3516720874743f797d416849012700499c980f203196e0516f9297ea74f5c55fd9f5fc261f3e0d900b4c458afdc005a2878a03ea0a0006696b7267a6c6a549c7c08cec998019186a7e76c69114a78f1cf086ff53eeb78f23348109682a7d364d2e676523005c7fd999bce43f204f4718e41161983813f48a5acc2672d43081cce35a233c4d810728344c3576205f0050733da8d8e1d0cb3c5fed94a171501b02d03deea8bef569b2ff637b3cfbc08ee00301540294aa9dfd54d483df52362a99c0778851b891d76178b31d9f71bd1f0f23a89d0a0007b40e7ec32188142c6d96ea16e996b227ec108da7d2b375fdd9471c7e8b0ac82b041f820965257437642b25287d0072bb026361b36f628c19c2c8bc960f5bfee210482744e4b5736fe233357083a509336b2f355234377d2500a781b7ece205567f42710822967a9e5625d2923f26acae891910636e0bcbb2e3";
    let input = input.to_string();

    let bytes = string_to_bytes(input);

    let proof = StreamProof::of_bin(&bytes, &mut 0);
    println!("proofs: {}", proof);

    let bytes0 = StreamProof::to_bin(proof.clone());
    println!("bytes: {:?}", Bytes::new(bytes0.clone()));

    let proof0 = StreamProof::of_bin(&bytes0, &mut 0);
    println!("proofs: {}", proof0);

    assert!(proof == proof0);
}

#[test]
fn v2_tree2_tree_test() {
    //data: {"version":0,"before":{"Value":{"base64":"KMhsVneFWKV7auTgUhUzn5RB4J57elK0ISbAdtBGCb8="}},"after":{"Node":{"base64":"DHsFgl8Ce7+jcj82bIBxTM0RcQTiUf1K71OyV15LXw="}},"state":{"Inode":{"length":658696,"proofs":[[0,{"Inode_tree":{"length":605225,"proofs":[[0,{"Inode_values":[["ij:\nV!oi_[,{"Node":[["$cqr",{"Blinded_node":{"base64":"LvKRyRasCHCrcb4jGBpaMgDM/IJc2lt9Jzhr6JT57q4="}}]]}],["RV t",{"Inode":{"length":471367,"proofs":[[0,{Inode_tree":{"length":24090,"proofs":[[0,{"Blinded_inode":{"base64":"hc2QjPurMD0wZOOyDuZWopT+CCgAfng+rCowZJumrjI="}}],[1,{"Blinded_inode":{"base6":"fO+shjobFKijkAa2wCNhfujMHt+uIkB5yb17kS+Nogk="}}]]}}]]}}]]}]]}}],[1,{"Blinded_inode":{"base64":"VORb25/Syy6D+vK3j2OkBM1CIFNbs4GVORlX55uGn7g="}}]}}}
    let input = "02000028c86c56778558a57b6ae4e05215339f9441e09e7b7a52b42126c076d04609bf0c7b05825f027bbfa3723f366c80714ccd2945c4138947f52bbd4ec95d792d7c02000a0d080200093c29820a696a3a0a56216f695f5b810424637172d02ef291c916ac0870ab71be23181a5a3200ccfc825cda5b7d27386be894f9eeae04525620740200073147015e1ac085cd908cfbab303d3064e3b20ee656a294fe0828007e783eac2a30649ba6ae32c07cefac863a1b14a8a39006b6c023617ee8cc1edfae224079c9bd7b912f8da209e0e0c054e45bdb9fd2cb2e83faf2b78f63a404cd4220535bb38195391957e79b869fb8";
    let input = input.to_string();

    let bytes = string_to_bytes(input);

    let proof = TreeProof::of_bin(&bytes, &mut 0);
    println!("proofs: {}", proof);

    let bytes0 = TreeProof::to_bin(proof.clone());
    println!("bytes: {:?}", Bytes::new(bytes0.clone()));

    let proof0 = TreeProof::of_bin(&bytes0, &mut 0);
    println!("proofs: {}", proof0);

    assert!(proof == proof0);
}
