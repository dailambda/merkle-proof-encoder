use crate::v2_tree2::*;

impl Elt {
    fn value_of_bin(bin: &[u8], off: &mut usize) -> Elt {
        let tag = u8::of_bin(bin, off);
        let tag = tag & 0b00000011u8;
        let p = if tag == 0 {
            1
        } else if tag == 1 {
            2
        } else {
            4
        };
        let length = usize_of_bin(bin, off, p);
        let content = bytes_range(bin, off, length).to_vec();
        Elt::Value(Bytes(content))
    }

    fn node_of_bin(bin: &[u8], off: &mut usize) -> Elt {
        let tag = u8::of_bin(bin, off);
        let length = (tag & 0b00000011u8) as usize;
        let mut v = vec![];
        for _i in 0..length {
            let step = Step::of_bin(bin, off);
            let kinded_hash = KindedHash::of_bin(bin, off);
            v.push((step, kinded_hash))
        }
        Elt::Node(v)
    }

    fn inode_of_bin(bin: &[u8], off: &mut usize) -> Elt {
        let tag = u8::of_bin(bin, off);
        let tag_for_len = tag & 0b00000011u8;
        let p = 1 << tag_for_len;
        let length = usize_of_bin(bin, off, p) as u64;

        let op0 = tag & 0b00000100u8;
        let op1 = tag & 0b00001000u8;
        let mut v = vec![];
        if op0 > 0 {
            let hash = Hash::of_bin(bin, off);
            v.push((0, hash));
        }
        if op1 > 0 {
            let hash = Hash::of_bin(bin, off);
            v.push((1, hash));
        }

        Elt::Inode(Inode::<Hash> { length, proofs: v })
    }

    fn inode_extender_of_bin(bin: &[u8], off: &mut usize) -> Elt {
        let tag = u8::of_bin(bin, off);
        let tag_for_len = tag & 0b00000011u8;
        let p = 1 << tag_for_len;
        let length = usize_of_bin(bin, off, p) as u64;

        let segment = Segment::of_bin(bin, off);
        let proof = Hash::of_bin(bin, off);

        Elt::InodeExtender(InodeExtender::<Hash> {
            length,
            segment,
            proof,
        })
    }

    fn value_to_bin(src: Bytes) -> RetBytes {
        let mut v = vec![];
        let Bytes(value) = src;
        let length = value.len();
        //0b110000yy
        let tag = 0b11000000u8;
        if length < 1 << 8 {
            v.push(tag);
            v.push(length as u8)
        } else if length < (1 << 16) {
            let tag = tag | 0b01;
            v.push(tag);
            v.append(&mut (length as u16).to_bin())
        } else {
            let tag = tag | 0b11;
            v.push(tag);
            v.append(&mut (length as u32).to_bin())
        }
        v.extend(value);
        v
    }

    fn node_to_bin(src: Vec<(Step, KindedHash)>) -> RetBytes {
        let mut v = vec![];
        //src.len <= 2
        let length = cmp::min(src.len() as u8, 3u8);
        //0b100000yy
        let tag = 0b10000000u8 | length;
        v.push(tag);
        if length >= 3 {
            v.append(&mut (src.len() as u32).to_bin())
        }
        for (s, k) in src.into_iter() {
            v.append(&mut s.to_bin());
            v.append(&mut k.to_bin())
        }
        v
    }

    fn inode_to_bin(Inode { length, proofs }: Inode<Hash>) -> RetBytes {
        let mut v = vec![];
        let (h0, h1) = proofs.into_iter().fold((None, None), |(h0, h1), (i, p)| {
            if i == 0 {
                (Some(p), h1)
            } else if i == 1 {
                (h0, Some(p))
            } else {
                (h0, h1)
            }
        });

        //0b0000zwyy
        let tag = (h0.as_ref().map_or(0b0u8, |_| 0b1u8) << 2)
            | (h1.as_ref().map_or(0b0u8, |_| 0b1u8) << 3);
        let (mut length, ltag) = usize_to_bin(length as usize);
        v.push(tag | ltag);
        v.append(&mut length);

        h0.map(|hash| v.append(&mut hash.to_bin()));
        h1.map(|hash| v.append(&mut hash.to_bin()));
        v
    }

    fn inode_extender_to_bin(
        InodeExtender {
            length,
            segment,
            proof,
        }: InodeExtender<Hash>,
    ) -> RetBytes {
        let mut v = vec![];
        //0b111000yy
        let tag = 0b11100000u8;
        let (mut length, ltag) = usize_to_bin(length as usize);
        v.push(tag | ltag);
        v.append(&mut length);
        v.append(&mut segment.to_bin());
        v.append(&mut proof.to_bin());
        v
    }
}

impl Encoding for Elt {
    fn of_bin(bin: &[u8], off: &mut usize) -> Elt {
        let tag = bin[*off];
        if tag & 0b10000000u8 > 0 {
            if tag & 0b01000000u8 > 0 {
                if tag & 0b00100000u8 > 0 {
                    //InodeExtender
                    Elt::inode_extender_of_bin(bin, off)
                } else {
                    //Value
                    Elt::value_of_bin(bin, off)
                }
            } else {
                //Node
                Elt::node_of_bin(bin, off)
            }
        } else {
            //Inode
            Elt::inode_of_bin(bin, off)
        }
    }

    fn to_bin(self: Elt) -> RetBytes {
        use Elt::*;
        match self {
            Value(val) => Elt::value_to_bin(val),
            Node(subtrees) => Elt::node_to_bin(subtrees),
            Inode(inode_proofs) => Elt::inode_to_bin(inode_proofs),
            InodeExtender(inode_extender) => Elt::inode_extender_to_bin(inode_extender),
        }
    }
}

impl Encoding for StreamProof {
    fn of_bin(bin: &[u8], off: &mut usize) -> StreamProof {
        let tag = u8::of_bin(bin, off);
        let version = u16::of_bin(bin, off);

        use KindedHash::{Node, Value};
        let before = {
            let hash = Hash::of_bin(bin, off);
            if tag & 0b01u8 > 0 {
                Node(hash)
            } else {
                Value(hash)
            }
        };
        let after = {
            let hash = Hash::of_bin(bin, off);
            if tag & 0b10u8 > 0 {
                Node(hash)
            } else {
                Value(hash)
            }
        };
        let bytelen = u32::of_bin(bin, off);

        let mut state = vec![];
        let bin = bytes_range(bin, off, bytelen as usize);
        let mut off = 0;
        loop {
            let elt = Elt::of_bin(&bin, &mut off);
            state.push(elt);
            if bin.len() <= off {
                break;
            }
        }
        StreamProof {
            version,
            before,
            after,
            state,
        }
    }

    fn to_bin(self: StreamProof) -> RetBytes {
        let StreamProof {
            version,
            before,
            after,
            state,
        } = self;
        let tag = before.get_tag() | (after.get_tag() << 1);
        let mut version = version.to_bin();
        let mut before = before.get_hash().unwrap();
        let mut after = after.get_hash().unwrap();
        let mut elts = state
            .into_iter()
            .map(|elt| elt.to_bin())
            .flatten()
            .collect::<Vec<u8>>();
        let length = elts.len() as u32;
        let mut length = (length as u32).to_bin();
        let mut v = vec![tag];
        v.append(&mut version);
        v.append(&mut before);
        v.append(&mut after);
        v.append(&mut length);
        v.append(&mut elts);
        v
    }
}
