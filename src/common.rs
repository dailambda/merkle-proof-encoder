pub(crate) fn bytes_range<'a>(bin: &'a [u8], off: &'a mut usize, len: usize) -> &'a [u8] {
    let l = *off;
    *off = *off + len;
    let r = *off;
    &bin[l..r]
}

impl Encoding for u8 {
    fn of_bin(bin: &[u8], off: &mut usize) -> u8 {
        let b = bin[*off] as u8;
        *off += 1;
        b
    }
    fn to_bin(self: u8) -> RetBytes {
        vec![self]
    }
}

impl Encoding for u16 {
    fn of_bin(bin: &[u8], off: &mut usize) -> u16 {
        let bin = bytes_range(bin, off, 2);
        let b0 = bin[0] as u16;
        let b1 = bin[1] as u16;
        (b0 << 8) + b1
    }
    fn to_bin(self: u16) -> RetBytes {
        let b0 = (self >> 8) as u8;
        let b1 = (self & ((1 << 8) - 1)) as u8;
        vec![b0, b1]
    }
}

impl Encoding for u32 {
    fn of_bin(bin: &[u8], off: &mut usize) -> u32 {
        let bin = bytes_range(bin, off, 4);
        let b0 = bin[0] as u32;
        let b1 = bin[1] as u32;
        let b2 = bin[2] as u32;
        let b3 = bin[3] as u32;
        b0 * (1 << 24) + b1 * (1 << 16) + b2 * (1 << 8) + b3
    }
    fn to_bin(self: u32) -> RetBytes {
        let mask = (1 << 8) - 1;
        let mut src = self;
        let b3 = (src & mask) as u8;
        src >>= 8;
        let b2 = (src & mask) as u8;
        src >>= 8;
        let b1 = (src & mask) as u8;
        src >>= 8;
        let b0 = (src & mask) as u8;
        vec![b0, b1, b2, b3]
    }
}

impl Encoding for u64 {
    fn of_bin(bin: &[u8], off: &mut usize) -> u64 {
        let bin = bytes_range(bin, off, 8);
        let mut r: u64 = 0;
        for i in bin {
            r = *i as u64 + (r << 8)
        }
        r
    }
    fn to_bin(self: u64) -> RetBytes {
        let mut v = vec![];
        let mut src = self;
        let mask = (1 << 8) - 1;
        for _ in 0..8 {
            let b = src & mask;
            v.push(b as u8);
            src >>= 8;
        }
        v.reverse();
        v
    }
}

pub(crate) fn usize_of_bin(bin: &[u8], off: &mut usize, size: usize) -> usize {
    let bin = bytes_range(bin, off, size);
    let mut r: usize = 0;
    for i in bin {
        r = *i as usize + (r << 8)
    }
    r
}

#[allow(dead_code)]
pub(crate) fn usize_to_bin(src: usize) -> (RetBytes, u8) {
    if src < (1 << 8) {
        (vec![src as u8], 0)
    } else if src < (1 << 16) {
        let r = (src as u16).to_bin();
        (r, 1)
    } else if src < (1 << 32) {
        let r = (src as u32).to_bin();
        (r, 2)
    } else {
        let r = (src as u64).to_bin();
        (r, 3)
    }
}

impl Encoding for Bytes {
    fn of_bin(bin: &[u8], off: &mut usize) -> Bytes {
        let length = u32::of_bin(bin, off);
        let content = bytes_range(bin, off, length as usize).to_vec();
        Bytes(content)
    }
    fn to_bin(self: Bytes) -> RetBytes {
        let Bytes(mut src) = self;
        let length = src.len() as u32;
        let mut v = length.to_bin();
        v.append(&mut src);
        v
    }
}

impl Hash {
    pub(crate) fn unwrap(self) -> Vec<u8> {
        self.0 .0
    }
}

impl Encoding for Hash {
    fn of_bin(bin: &[u8], off: &mut usize) -> Hash {
        let hash = bytes_range(bin, off, HASH_LEN).to_vec();
        Hash(Bytes(hash))
    }
    fn to_bin(self: Hash) -> RetBytes {
        let Hash(Bytes(src)) = self;
        src
    }
}

impl KindedHash {
    #[allow(dead_code)]
    pub(crate) fn get_tag(&self) -> u8 {
        use KindedHash::*;
        match self {
            Value(_) => 0,
            Node(_) => 1,
        }
    }

    #[allow(dead_code)]
    pub(crate) fn get_hash(self) -> Hash {
        use KindedHash::*;
        match self {
            Value(hash) => hash,
            Node(hash) => hash,
        }
    }
}

impl Encoding for KindedHash {
    fn of_bin(bin: &[u8], off: &mut usize) -> KindedHash {
        let tag = u8::of_bin(bin, off);
        let hash = Hash::of_bin(bin, off);
        if tag & 0b00000001u8 == 0 {
            KindedHash::Value(hash)
        } else {
            KindedHash::Node(hash)
        }
    }
    fn to_bin(self: KindedHash) -> RetBytes {
        use KindedHash::*;
        match self {
            Value(hash) => {
                let mut v = vec![0];
                v.extend(hash.unwrap());
                v
            }
            Node(hash) => {
                let mut v = vec![1u8];
                v.extend(hash.unwrap());
                v
            }
        }
    }
}

impl Encoding for Step {
    fn of_bin(bin: &[u8], off: &mut usize) -> Step {
        let length = u8::of_bin(bin, off) as usize;
        let content = bytes_range(bin, off, length);
        let s = content.iter().map(|c| *c as char).collect::<String>();
        Step(s)
    }
    fn to_bin(self: Step) -> RetBytes {
        let Self(src) = self;
        let length = src.len() as u8;
        let content = src.as_bytes().to_vec();
        let mut v = RetBytes::new();
        v.push(length);
        v.extend(content);
        v
    }
}

impl<const N: usize> Encoding for Seg<N> {
    //N<8
    fn of_bin(bin: &[u8], off: &mut usize) -> Seg<N> {
        use bitvec::prelude::*;
        let length = u8::of_bin(bin, off) as usize;
        let bin = bytes_range(bin, off, length);
        let bs: &BitSlice<u8, Msb0> = &BitVec::from_slice(bin);
        let chunks = bs.chunks(N);

        let mut v = vec![];
        let mut buf = vec![];
        for c in chunks {
            let x = c.load_be::<u8>();
            if x > 0 {
                v.append(&mut buf);
                buf.clear();
            }
            buf.push(x);
        }
        Seg(v)
    }
    fn to_bin(self: Seg<N>) -> RetBytes {
        use bitvec::prelude::*;
        let Seg(src) = self;
        let mut bv = bitvec![u8, Msb0;];
        for x in src.into_iter() {
            let p = &x.view_bits::<Msb0>()[8 - N..];
            bv.extend(p)
        }
        bv.push(true);

        let mut bv = bv.into_vec();
        let mut v = vec![bv.len() as u8];
        v.append(&mut bv);
        v
    }
}
