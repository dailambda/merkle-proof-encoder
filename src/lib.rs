fn to_bin(b: u8) -> u8 {
    if b'0' <= b && b <= b'9' {
        b - b'0'
    } else if b'a' <= b && b <= b'f' {
        b - b'a' + 10
    } else {
        assert!(false);
        0 as u8
    }
}

#[allow(dead_code)]
pub(crate) fn string_to_bytes(str: String) -> Vec<u8> {
    let mut b = Vec::new();
    let input = str.as_bytes();
    for i in (0..str.len()).step_by(2) {
        let c1 = to_bin(input[i]);
        let c2 = to_bin(input[i + 1]);
        b.push(c1 * 16 + c2)
    }
    b
}
