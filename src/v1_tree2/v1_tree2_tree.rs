use crate::v1_tree2::*;

type InodeProofs = Vec<(Index, InodeTree)>;
fn inode_proofs_of_bin(bin: &[u8], off: &mut usize) -> InodeProofs {
    let mut v = vec![];
    (0..2).for_each(|i| Option::<InodeTree>::of_bin(bin, off).map_or((), |it| v.push((i, it))));
    v
}
fn inode_proofs_to_bin(proofs: InodeProofs) -> RetBytes {
    let (it0, it1) = proofs.into_iter().fold((None, None), |(it0, it1), (i, p)| {
        if i == 0 {
            (Some(p), it1)
        } else if i == 1 {
            (it0, Some(p))
        } else {
            (it0, it1)
        }
    });
    let mut v = it0.to_bin();
    v.append(&mut it1.to_bin());
    v
}

impl InodeTree {
    fn blinded_inode_of_bin(bin: &[u8], off: &mut usize) -> InodeTree {
        let hash = Hash::of_bin(bin, off);
        InodeTree::BlindedInode(hash)
    }
    fn blinded_inode_to_bin(src: Hash) -> RetBytes {
        src.to_bin()
    }

    fn inode_values_of_bin(bin: &[u8], off: &mut usize) -> InodeTree {
        let length = usize_of_bin(bin, off, 4);
        let r = *off + length; //byte length
        let mut v = vec![];
        while *off < r {
            let step = Step::of_bin(bin, off);
            let tree = Tree::of_bin(bin, off);
            v.push((step, tree))
        }
        InodeTree::InodeValues(v)
    }
    fn inode_values_to_bin(src: Vec<(Step, Tree)>) -> RetBytes {
        let len_size = 4;
        let mut v = vec![0; len_size];
        for (s, t) in src.into_iter() {
            v.append(&mut s.to_bin());
            v.append(&mut t.to_bin())
        }
        let pv = ((v.len() - len_size) as u32).to_bin();
        for i in 0..len_size {
            v[i] = pv[i]
        }
        v
    }

    fn inode_trees_of_bin(bin: &[u8], off: &mut usize) -> InodeTree {
        let length = usize_of_bin(bin, off, 8) as u64;
        let proofs = inode_proofs_of_bin(bin, off);
        let inode_trees = Box::<Inode<InodeTree>>::new(Inode::<InodeTree> { length, proofs });
        InodeTree::InodeTrees(inode_trees)
    }
    fn inode_trees_to_bin(Inode { length, proofs }: Inode<InodeTree>) -> RetBytes {
        let mut v = length.to_bin();
        v.append(&mut inode_proofs_to_bin(proofs));
        v
    }

    fn inode_extender_of_bin(bin: &[u8], off: &mut usize) -> InodeTree {
        let length = usize_of_bin(bin, off, 8) as u64;
        let segment = Segment::of_bin(bin, off);
        let proof = InodeTree::of_bin(bin, off);
        let inode_extender = Box::<InodeExtender<InodeTree>>::new(InodeExtender::<InodeTree> {
            length,
            segment,
            proof,
        });
        InodeTree::InodeExtender(inode_extender)
    }
    fn inode_extender_to_bin(
        InodeExtender {
            length,
            segment,
            proof,
        }: InodeExtender<InodeTree>,
    ) -> RetBytes {
        let mut v = length.to_bin();
        v.append(&mut segment.to_bin());
        v.append(&mut proof.to_bin());
        v
    }
}

impl Encoding for InodeTree {
    fn of_bin(bin: &InBytes, off: &mut usize) -> InodeTree {
        let tag = u8::of_bin(bin, off);
        let tree = match tag {
            0 => InodeTree::blinded_inode_of_bin(bin, off),
            1 => InodeTree::inode_values_of_bin(bin, off),
            2 => InodeTree::inode_trees_of_bin(bin, off),
            3 => InodeTree::inode_extender_of_bin(bin, off),
            _ => panic!(),
        };
        tree
    }
    fn to_bin(self) -> RetBytes {
        use InodeTree::*;
        let (tag, mut content) = match self {
            BlindedInode(hash) => (0, InodeTree::blinded_inode_to_bin(hash)),
            InodeValues(subtree) => (1, InodeTree::inode_values_to_bin(subtree)),
            InodeTrees(inode_proofs) => (2, InodeTree::inode_trees_to_bin(*inode_proofs)),
            InodeExtender(ext) => (3, InodeTree::inode_extender_to_bin(*ext)),
        };
        let mut v = vec![tag];
        v.append(&mut content);
        v
    }
}

impl Encoding for Option<InodeTree> {
    fn of_bin(bin: &InBytes, off: &mut usize) -> Option<InodeTree> {
        let tag = bin[*off];
        if tag == 4u8 {
            *off += 1;
            None
        } else {
            Some(InodeTree::of_bin(bin, off))
        }
    }
    fn to_bin(self) -> RetBytes {
        match self {
            None => {
                vec![4u8]
            }
            Some(it) => it.to_bin(),
        }
    }
}

impl Tree {
    fn value_of_bin(bin: &InBytes, off: &mut usize) -> Tree {
        let value = Bytes::of_bin(bin, off);
        Tree::Value(value)
    }
    fn value_to_bin(src: Bytes) -> RetBytes {
        src.to_bin()
    }

    fn blinded_value_of_bin(bin: &[u8], off: &mut usize) -> Tree {
        let hash = Hash::of_bin(bin, off);
        Tree::BlindedValue(hash)
    }
    fn blinded_value_to_bin(src: Hash) -> RetBytes {
        src.to_bin()
    }

    fn node_of_bin(bin: &[u8], off: &mut usize) -> Tree {
        let length = usize_of_bin(bin, off, 4);
        let r = *off + length; //byte length
        let mut v = vec![];
        while *off < r {
            let step = Step::of_bin(bin, off);
            let tree = Tree::of_bin(bin, off);
            v.push((step, tree))
        }
        Tree::Node(v)
    }
    fn node_to_bin(src: Vec<(Step, Tree)>) -> RetBytes {
        let len_size = 4;
        let mut v = vec![0; len_size];
        for (s, t) in src.into_iter() {
            v.append(&mut s.to_bin());
            v.append(&mut t.to_bin())
        }
        let pv = ((v.len() - len_size) as u32).to_bin();
        for i in 0..len_size {
            v[i] = pv[i]
        }
        v
    }

    fn blinded_node_of_bin(bin: &[u8], off: &mut usize) -> Tree {
        let hash = Hash::of_bin(bin, off);
        Tree::BlindedNode(hash)
    }
    fn blinded_node_to_bin(src: Hash) -> RetBytes {
        src.to_bin()
    }

    fn inode_of_bin(bin: &[u8], off: &mut usize) -> Tree {
        let length = usize_of_bin(bin, off, 8) as u64;
        let proofs = inode_proofs_of_bin(bin, off);
        Tree::Inode(Inode::<InodeTree> { length, proofs })
    }
    fn inode_to_bin(Inode { length, proofs }: Inode<InodeTree>) -> RetBytes {
        let mut v = length.to_bin();
        v.append(&mut inode_proofs_to_bin(proofs));
        v
    }

    fn extender_of_bin(bin: &[u8], off: &mut usize) -> Tree {
        let length = usize_of_bin(bin, off, 8) as u64;
        let segment = Segment::of_bin(bin, off);
        let proof = InodeTree::of_bin(bin, off);
        Tree::Extender(InodeExtender::<InodeTree> {
            length,
            segment,
            proof,
        })
    }
    fn extender_to_bin(
        InodeExtender {
            length,
            segment,
            proof,
        }: InodeExtender<InodeTree>,
    ) -> RetBytes {
        let mut v = (length as u64).to_bin();
        v.append(&mut segment.to_bin());
        v.append(&mut proof.to_bin());
        v
    }
}

impl Encoding for Tree {
    fn of_bin(bin: &InBytes, off: &mut usize) -> Tree {
        let tag = u8::of_bin(bin, off);
        let tree = match tag {
            0 => Tree::value_of_bin(bin, off),
            1 => Tree::blinded_value_of_bin(bin, off),
            2 => Tree::node_of_bin(bin, off),
            3 => Tree::blinded_node_of_bin(bin, off),
            4 => Tree::inode_of_bin(bin, off),
            5 => Tree::extender_of_bin(bin, off),
            _ => panic!(),
        };
        tree
    }
    fn to_bin(self) -> RetBytes {
        use Tree::*;
        let (tag, mut content) = match self {
            Value(v) => (0, Tree::value_to_bin(v)),
            BlindedValue(hash) => (1, Tree::blinded_value_to_bin(hash)),
            Node(subtree) => (2, Tree::node_to_bin(subtree)),
            BlindedNode(hash) => (3, Tree::blinded_node_to_bin(hash)),
            Inode(inode_proofs) => (4, Tree::inode_to_bin(inode_proofs)),
            Extender(ext) => (5, Tree::extender_to_bin(ext)),
        };
        let mut v = vec![tag];
        v.append(&mut content);
        v
    }
}

impl Encoding for TreeProof {
    fn of_bin(bin: &InBytes, off: &mut usize) -> TreeProof {
        let version = u16::of_bin(bin, off);
        let before = KindedHash::of_bin(bin, off);
        let after = KindedHash::of_bin(bin, off);

        let state = Tree::of_bin(&bin, off);
        TreeProof {
            version,
            before,
            after,
            state,
        }
    }
    fn to_bin(self) -> RetBytes {
        let TreeProof {
            version,
            before,
            after,
            state,
        } = self;
        let version = version.to_bin();
        let mut before = before.to_bin();
        let mut after = after.to_bin();
        let mut state = state.to_bin();
        let mut v = version;
        v.append(&mut before);
        v.append(&mut after);
        v.append(&mut state);
        v
    }
}
