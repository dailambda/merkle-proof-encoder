use crate::v1_tree2::*;

impl Encoding for Option<Hash> {
    fn of_bin(bin: &InBytes, off: &mut usize) -> Option<Hash> {
        let tag = bytes_range(bin, off, 1)[0];
        if tag > 0 {
            Some(Hash::of_bin(bin, off))
        } else {
            None
        }
    }
    fn to_bin(self) -> RetBytes {
        match self {
            None => {
                let tag = 0u8;
                vec![tag]
            }
            Some(hash) => {
                let tag = 1u8;
                let mut v = vec![tag];
                v.append(&mut hash.to_bin());
                v
            }
        }
    }
}

type InodeProofsOfHash = Vec<(Index, Hash)>;
fn inode_proofs_of_bin(bin: &[u8], off: &mut usize) -> InodeProofsOfHash {
    let mut v = vec![];
    (0..2).for_each(|i| Option::<Hash>::of_bin(bin, off).map_or((), |it| v.push((i, it))));
    v
}
fn inode_proofs_to_bin(proofs: InodeProofsOfHash) -> RetBytes {
    let (h0, h1) = proofs.into_iter().fold((None, None), |(h0, h1), (i, p)| {
        if i == 0 {
            (Some(p), h1)
        } else if i == 1 {
            (h0, Some(p))
        } else {
            (h0, h1)
        }
    });
    let mut v = h0.to_bin();
    v.append(&mut h1.to_bin());
    v
}

impl Elt {
    fn value_of_bin(bin: &[u8], off: &mut usize) -> Elt {
        let value = Bytes::of_bin(bin, off);
        Elt::Value(value)
    }
    fn value_to_bin(src: Bytes) -> RetBytes {
        src.to_bin()
    }

    fn node_of_bin(bin: &[u8], off: &mut usize) -> Elt {
        let length = usize_of_bin(bin, off, 4) as usize;
        let r = *off + length;
        let mut v = vec![];
        while *off < r {
            let step = Step::of_bin(bin, off);
            let kinded_hash = KindedHash::of_bin(bin, off);
            v.push((step, kinded_hash))
        }
        Elt::Node(v)
    }
    fn node_to_bin(src: Vec<(Step, KindedHash)>) -> RetBytes {
        let len_size = 4;
        let mut v = vec![0; len_size]; //byte length
        for (s, k) in src.into_iter() {
            v.append(&mut s.to_bin());
            v.append(&mut k.to_bin())
        }
        let pv = ((v.len() - len_size) as u32).to_bin();
        for i in 0..len_size {
            v[i] = pv[i]
        }
        v
    }

    fn inode_of_bin(bin: &[u8], off: &mut usize) -> Elt {
        let length = usize_of_bin(bin, off, 8) as u64;
        let proofs = inode_proofs_of_bin(bin, off);
        Elt::Inode(Inode::<Hash> { length, proofs })
    }
    fn inode_to_bin(Inode { length, proofs }: Inode<Hash>) -> RetBytes {
        let mut v = length.to_bin();
        v.append(&mut inode_proofs_to_bin(proofs));
        v
    }

    fn inode_extender_of_bin(bin: &[u8], off: &mut usize) -> Elt {
        let length = usize_of_bin(bin, off, 8) as u64;
        let segment = Segment::of_bin(bin, off);
        let proof = Hash::of_bin(bin, off);

        Elt::InodeExtender(InodeExtender::<Hash> {
            length,
            segment,
            proof,
        })
    }
    fn inode_extender_to_bin(
        InodeExtender {
            length,
            segment,
            proof,
        }: InodeExtender<Hash>,
    ) -> RetBytes {
        let mut v = length.to_bin();
        v.append(&mut segment.to_bin());
        v.append(&mut proof.to_bin());
        v
    }
}

impl Encoding for Elt {
    fn of_bin(bin: &[u8], off: &mut usize) -> Elt {
        let tag = bytes_range(bin, off, 1)[0];
        match tag {
            0 => Elt::value_of_bin(bin, off),
            1 => Elt::node_of_bin(bin, off),
            2 => Elt::inode_of_bin(bin, off),
            3 => Elt::inode_extender_of_bin(bin, off),
            _ => panic!(),
        }
    }
    fn to_bin(self: Elt) -> RetBytes {
        use Elt::*;
        let (tag, mut content) = match self {
            Value(val) => (0, Elt::value_to_bin(val)),
            Node(subtrees) => (1, Elt::node_to_bin(subtrees)),
            Inode(inode_proofs) => (2, Elt::inode_to_bin(inode_proofs)),
            InodeExtender(inode_extender) => (3, Elt::inode_extender_to_bin(inode_extender)),
        };
        let mut v = vec![tag];
        v.append(&mut content);
        v
    }
}

impl Encoding for StreamProof {
    fn of_bin(bin: &[u8], off: &mut usize) -> StreamProof {
        let version = u16::of_bin(bin, off);
        let before = KindedHash::of_bin(bin, off);
        let after = KindedHash::of_bin(bin, off);
        let bytelen = usize_of_bin(bin, off, 4);

        let mut state = vec![];
        let bin = bytes_range(bin, off, bytelen as usize);
        let mut off = 0;
        while off < bytelen {
            let elt = Elt::of_bin(&bin, &mut off);
            state.push(elt);
        }
        StreamProof {
            version,
            before,
            after,
            state,
        }
    }

    fn to_bin(self: StreamProof) -> RetBytes {
        let StreamProof {
            version,
            before,
            after,
            state,
        } = self;
        let version = version.to_bin();
        let mut before = before.to_bin();
        let mut after = after.to_bin();
        let mut elts = state
            .into_iter()
            .map(|elt| elt.to_bin())
            .flatten()
            .collect::<Vec<u8>>();
        let length = elts.len() as u32;
        let mut length = length.to_bin();
        let mut v = version;
        v.append(&mut before);
        v.append(&mut after);
        v.append(&mut length);
        v.append(&mut elts);
        v
    }
}
