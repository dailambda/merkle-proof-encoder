mod lib;
mod v1_tree2;
mod v1_tree32;
mod v2_tree2;
mod v2_tree32;
use crate::lib::*;
use std::fmt;

enum ProofType {
    V1Tree2Tree,
    V1Tree2Stream,
    V1Tree32Tree,
    V1Tree32Stream,
    V2Tree2Tree,
    V2Tree2Stream,
    V2Tree32Tree,
    V2Tree32Stream,
}

enum Proof {
    V1Tree2Tree(v1_tree2::TreeProof),
    V1Tree2Stream(v1_tree2::StreamProof),
    V1Tree32Tree(v1_tree32::TreeProof),
    V1Tree32Stream(v1_tree32::StreamProof),
    V2Tree2Tree(v2_tree2::TreeProof),
    V2Tree2Stream(v2_tree2::StreamProof),
    V2Tree32Tree(v2_tree32::TreeProof),
    V2Tree32Stream(v2_tree32::StreamProof),
}

impl Proof {
    fn of_bin(ptype: ProofType, bin: &Vec<u8>) -> Proof {
        use Proof::*;
        match ptype {
            ProofType::V1Tree2Tree => {
                use v1_tree2::*;
                V1Tree2Tree(TreeProof::of_bin(bin, &mut 0))
            }
            ProofType::V1Tree2Stream => {
                use v1_tree2::*;
                V1Tree2Stream(StreamProof::of_bin(bin, &mut 0))
            }
            ProofType::V1Tree32Tree => {
                use v1_tree32::*;
                V1Tree32Tree(TreeProof::of_bin(bin, &mut 0))
            }
            ProofType::V1Tree32Stream => {
                use v1_tree32::*;
                V1Tree32Stream(StreamProof::of_bin(bin, &mut 0))
            }
            ProofType::V2Tree2Tree => {
                use v2_tree2::*;
                V2Tree2Tree(TreeProof::of_bin(bin, &mut 0))
            }
            ProofType::V2Tree2Stream => {
                use v2_tree2::*;
                V2Tree2Stream(StreamProof::of_bin(bin, &mut 0))
            }
            ProofType::V2Tree32Tree => {
                use v2_tree32::*;
                V2Tree32Tree(TreeProof::of_bin(bin, &mut 0))
            }
            ProofType::V2Tree32Stream => {
                use v2_tree32::*;
                V2Tree32Stream(StreamProof::of_bin(bin, &mut 0))
            }
        }
    }
    fn to_bin(self) -> Vec<u8> {
        use Proof::*;
        match self {
            V1Tree2Tree(proof) => {
                use v1_tree2::*;
                proof.to_bin()
            }
            V1Tree2Stream(proof) => {
                use v1_tree2::*;
                proof.to_bin()
            }
            V1Tree32Tree(proof) => {
                use v1_tree32::*;
                proof.to_bin()
            }
            V1Tree32Stream(proof) => {
                use v1_tree32::*;
                proof.to_bin()
            }
            V2Tree2Tree(proof) => {
                use v2_tree2::*;
                proof.to_bin()
            }
            V2Tree2Stream(proof) => {
                use v2_tree2::*;
                proof.to_bin()
            }
            V2Tree32Tree(proof) => {
                use v2_tree32::*;
                proof.to_bin()
            }
            V2Tree32Stream(proof) => {
                use v2_tree32::*;
                proof.to_bin()
            }
        }
    }
}

impl fmt::Display for Proof {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use Proof::*;
        match self {
            V1Tree2Tree(proof) => write!(f, "{}", proof),
            V1Tree2Stream(proof) => write!(f, "{}", proof),
            V1Tree32Tree(proof) => write!(f, "{}", proof),
            V1Tree32Stream(proof) => write!(f, "{}", proof),
            V2Tree2Tree(proof) => write!(f, "{}", proof),
            V2Tree2Stream(proof) => write!(f, "{}", proof),
            V2Tree32Tree(proof) => write!(f, "{}", proof),
            V2Tree32Stream(proof) => write!(f, "{}", proof),
        }
    }
}

fn print_usage(program: &str) {
    let brief = format!("Usage:{} [v1|v2] [tree2|tree32] [tree|stream]", program);
    println!("{}", brief)
}

fn main() {
    let args: Vec<String> = std::env::args().collect();
    let (program, version, nodenum, prooftype) = match args.as_slice() {
        [p, v, n, t] => (p, v, n, t),
        [p, ..] => {
            print_usage(p);
            return;
        }
        _ => panic!(),
    };

    let mut input = String::new();
    std::io::stdin()
        .read_line(&mut input)
        .expect("Failed to read line.");
    input = input.trim().to_string();
    let bytes = string_to_bytes(input);

    use ProofType::*;
    let ptype = match (version.as_str(), nodenum.as_str(), prooftype.as_str()) {
        ("v1", "tree2", "tree") => V1Tree2Tree,
        ("v1", "tree2", "stream") => V1Tree2Stream,
        ("v1", "tree32", "tree") => V1Tree32Tree,
        ("v1", "tree32", "stream") => V1Tree32Stream,
        ("v2", "tree2", "tree") => V2Tree2Tree,
        ("v2", "tree2", "stream") => V2Tree2Stream,
        ("v2", "tree32", "tree") => V2Tree32Tree,
        ("v2", "tree32", "stream") => V2Tree32Stream,
        _ => {
            print_usage(program);
            return;
        }
    };
    let parsed = Proof::of_bin(ptype, &bytes);
    println!("proof:\n{}", parsed);

    let bytes = parsed.to_bin();
    let mut p = String::new();
    bytes
        .iter()
        .for_each(|x| p.push_str(&format!("{:02x}", x).to_string()));
    println!("bytes:\n{}", p)
}
