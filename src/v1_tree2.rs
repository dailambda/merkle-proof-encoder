mod v1_tree2_stream;
mod v1_tree2_tree;

include!("proof_type.rs");

type S = Seg<1>;
impl Encoding for Segment {
    fn of_bin(bin: &InBytes, off: &mut usize) -> Segment {
        Segment(S::of_bin(bin, off).0)
    }
    fn to_bin(self) -> RetBytes {
        let Segment(src) = self;
        S::to_bin(Seg(src))
    }
}

#[test]
fn tree2_segment_test() {
    let seg = Segment([0, 1, 0, 0, 1, 0, 0, 0, 1].to_vec());
    let bytes = seg.clone().to_bin();
    println!("{}", Bytes(bytes.clone()));
    let seg0 = Segment::of_bin(bytes.as_slice(), &mut 0);
    println!("{}", seg0);
}

#[test]
fn v1_tree2_stream_test() {
    //{"version":3,"before":{"Value":{"base64":"ahOTPNBbIockAOjcKegUCkbX6WD6PUAFypeHe7Yn5Sc="}},"after":{"Node":{"base64":"yG7ReHA6+6WTbwV0dxnPC61rSeTRSCq0Fbh5fwyqadA="}},"state":[{"Inode":{"length":544006,"proofs":[[0,{"base64":"W50Cdu8ehPIl+WlQ+N+Rm2p3MGpZyL70YOR6UQtV5Ms="}],[1,{"base64":"kYzyRW6aqqJx5i+Mk80jHxpipXY5TIHraNb/HtWLszg="}]]}},{"Inode_extender":{"length":4,"segment":[0,1,0,0],"proof":{"base64":"2B+ZEXBf4ln83FDto9m6qQRPdu6HMMPHqTS2dIoM3qM="}}},{"Inode_extender":{"length":9,"segment":[0],"proof":{"base64":"ah/b++7qNC5q43v6qm4Mzxt1OnIrpotQl0ik9pM/how="}}},{"Inode_extender":{"length":1,"segment":[1,1,0,0,0],"proof":{"base64":"uSL1EXsxUyGdWBtBPytjR5cEfLpmMRMFwPHDi+Z0Bmc="}}},{"Inode":{"length":933903,"proofs":[[0,{"base64":"7QAmltlPpPuTIOeAHxoM+b5DVhHOLcq2Wo/J9/cjgyg="}],[1,{"base64":"ThCIvYfSqNUna3JwcC08dJySP2b4dC9NaFEMDGgbaLA="}]]}},{"Node":[["\nRER}8C2\n",{"Value":{"base64":"WAT0Zmh7bLJ4z9tMAKuF9zlaE/EdO+x+2mR0BQy/zMA="}}]]}]}
    let input = "0003006a13933cd05b22872400e8dc29e8140a46d7e960fa3d4005ca97877bb627e52701c86ed178703afba5936f05747719cf0bad6b49e4d1482ab415b8797f0caa69d000000147020000000000084d06015b9d0276ef1e84f225f96950f8df919b6a77306a59c8bef460e47a510b55e4cb01918cf2456e9aaaa271e62f8c93cd231f1a62a576394c81eb68d6ff1ed58bb3380300000000000000040148d81f9911705fe259fcdc50eda3d9baa9044f76ee8730c3c7a934b6748a0cdea303000000000000000901406a1fdbfbeeea342e6ae37bfaaa6e0ccf1b753a722ba68b509748a4f6933f868c03000000000000000101c4b922f5117b3153219d581b413f2b634797047cba66311305c0f1c38be67406670200000000000e400f01ed002696d94fa4fb9320e7801f1a0cf9be435611ce2dcab65a8fc9f7f7238328014e1088bd87d2a8d5276b7270702d3c749c923f66f8742f4d68510c0c681b68b0010000002b090a5245527d3843320a005804f466687b6cb278cfdb4c00ab85f7395a13f11d3bec7eda6474050cbfccc0";
    let input = input.to_string();

    let bytes = string_to_bytes(input);

    let proof = StreamProof::of_bin(&bytes, &mut 0);
    println!("proofs: {}", proof);

    let bytes0 = StreamProof::to_bin(proof.clone());
    println!("bytes: {:?}", Bytes::new(bytes0.clone()));

    let proof0 = StreamProof::of_bin(&bytes0, &mut 0);
    println!("proofs: {}", proof0);

    assert!(proof == proof0);
}

#[test]
fn v1_tree2_tree_test() {
    //{"version":3,"before":{"Node":{"base64":"ds6hOPTIFVN5LO7T7zgaU6GwY6O3baKJ/k+2oAOZTig="}},"after":{"Value":{"base64":"HWmvRwh6REDKDQ9D/9ji8f+Ev6ypoGItYNXoZGZy3D8="}},"state":{"Extender":{"length":6,"segment":[1],"proof":{"Inode_values":[["Y9n",{"Inode":{"length":269453,"proofs":[[0,{"Inode_values":[["FQ;x",{"Blinded_node":{"base64":"cFhTJSPQ5wuaQ9w/J3/I4QvcxpniJW8lbWkqkBmavys="}}],["LtgMqx{W,",{"Inode":{"length":739821,"proofs":[[0,{"Blinded_inode":{"base64":"iroXcvcmB1E7UjJvvZVWIN/qTb2v0ff7jtOpbiGrs04="}}],[1,{"Blinded_inode":{"base64":"s4xYcpG8xn7I/cnQtZzXgTYPhBSwxPUfWfSHQO1u6ks="}}]]}}]]}],[1,{"Inode_tree":{"length":273686,"proofs":[[0,{"Inode_extender":{"length":2,"segment":[0,1,0,0,1,1],"proof":{"Blinded_inode":{"base64":"YB1ucggGNZSZQjkfljSG42Cp8TVvlIcrmna0PLPY58Y="}}}}],[1,{"Inode_tree":{"length":319615,"proofs":[[0,{"Blinded_inode":{"base64":"ZPpUnJAa8bGDvn+oBxN94d5mUNmB6d6AFvgJKWMzh+A="}}],[1,{"Blinded_inode":{"base64":"OO2MknY8icJ/7DSAKYc8inle+GR6WYYuUChfc7UnqbY="}}]]}}]]}}]]}}]]}}}}
    let input = "00030176cea138f4c81553792ceed3ef381a53a1b063a3b76da289fe4fb6a003994e28001d69af47087a4440ca0d0f43ffd8e2f1ff84bfaca9a0622d60d5e8646672dc3f05000000000000000601c0010000010d0359396e040000000000041c8d010000007b0446513b78037058532523d0e70b9a43dc3f277fc8e10bdcc699e2256f256d692a90199abf2b094c74674d71787b572c0400000000000b49ed008aba1772f72607513b52326fbd955620dfea4dbdafd1f7fb8ed3a96e21abb34e00b38c587291bcc67ec8fdc9d0b59cd781360f8414b0c4f51f59f48740ed6eea4b020000000000042d16030000000000000002014e00601d6e72080635949942391f963486e360a9f1356f94872b9a76b43cb3d8e7c602000000000004e07f0064fa549c901af1b183be7fa807137de1de6650d981e9de8016f80929633387e00038ed8c92763c89c27fec348029873c8a795ef8647a59862e50285f73b527a9b6";
    let input = input.to_string();

    let bytes = string_to_bytes(input);

    let proof = TreeProof::of_bin(&bytes, &mut 0);
    println!("proofs: {}", proof);

    let bytes0 = TreeProof::to_bin(proof.clone());
    println!("bytes: {:?}", Bytes::new(bytes0.clone()));

    let proof0 = TreeProof::of_bin(&bytes0, &mut 0);
    println!("proofs: {}", proof0);

    assert!(proof == proof0);
}
