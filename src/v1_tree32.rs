mod v1_tree32_stream;
mod v1_tree32_tree;

include!("proof_type.rs");

type S = Seg<5>;
impl Encoding for Segment {
    fn of_bin(bin: &InBytes, off: &mut usize) -> Segment {
        Segment(S::of_bin(bin, off).0)
    }
    fn to_bin(self) -> RetBytes {
        let Segment(src) = self;
        S::to_bin(Seg(src))
    }
}

#[test]
fn tree32_segment_test() {
    let seg = Segment([1, 2, 3, 4, 5, 6, 7, 10, 31].to_vec());
    let bytes = seg.clone().to_bin();
    println!("{}", Bytes(bytes.clone()));
    let seg0 = Segment::of_bin(bytes.as_slice(), &mut 0);
    println!("{}", seg0);
}

#[test]
fn v1_tree32_stream_test() {
    //{"version":3,"before":{"Value":{"base64":"Xf9/kSKegW0tehVs6Jq87T6NEDhN5uh9KhvkpnvCKLQ="}},"after":{"Node":{"base64":"QZ0m8LQsEEtOPmRzyQvx4Fa+H+LnnGsEWD24S7kiJ7Y="}},"state":[{"Value":{"base64":"T0nM"}},{"Inode_extender":{"length":7,"segment":[1,4,1,2,0],"proof":{"base64":"bEztQIkWLaIwgeE2So17ZnnGkLn06uEweX4xgObMvO8="}}},{"Inode":{"length":121211,"proofs":[[0,{"base64":"D/wRIiM2DClpBwAj7iAHQZrINh7Ou90uYnu5V5C63sE="}],[3,{"base64":"EOUg2NPmFYqg20xF5FSinBh1XN5XeR/7E3FX23uYs1I="}],[5,{"base64":"3fX35BslDdJRPMIsQzY7m5+Rqak2imtrJCx7WMtLgfI="}],[6,{"base64":"YZFQ1KDlNMsQAv00prfdXCp4EykZtBAP9p/SrqPZFbs="}],[7,{"base64":"mqk2XyELU8tBWaIkQSXjT89VPJucnl7Edz5qiIeMfFA="}],[8,{"base64":"IaQb4hwaXC28ukrkoI2bfZuncdLwhtKZnM0kbYNzRUI="}],[9,{"base64":"9n3usrBlsHgk5Z5QWBCuMAyfC6yQuIB4rzTtcroFx8o="}],[10,{"base64":"GsirGB+X/HbHsKlpLql78xjBDfeVi8h5YeWQ21QuG1I="}],[11,{"base64":"UB5CmMgpSQ4iNNfVjB5tI75lKCUls5CyHEeVAw4d5RA="}],[12,{"base64":"dG2mqT0MLMLJ9ZYJVfVU/lyK18/cBCZAPbOb6df+7hs="}],[13,{"base64":"bg46U7K3dpmr6mKVX7Tnn1k8lMSei8d81yGMQEYyj10="}],[14,{"base64":"1bkuYg/78/jqaTRS45mUiQvfnAK8vQa0sTf5EC1VOqw="}],[15,{"base64":"KUiBbPtXrj7fuOzGSLDCYK1etwilwiVeiGGilBzglhg="}],[16,{"base64":"kss2KyQyWf3pizzeaLlUugolfmYfP88wxLI2glbrjqA="}],[17,{"base64":"zk+864vr6I8OVglfR8XxtENWdAFRhVIfXIL5SOps//Q="}],[18,{"base64":"rK/YjSNBdUUgWRhTtyBU3+SOiMaaoNqzbqW/bgyR87E="}],[19,{"base64":"8cDWHRhCzGoGHmYwzVFjumGwLrS5MXwIyq3Zo2W9s4U="}],[21,{"base64":"g4xK85ztmumV8MShYPNwDDcG+vCVermeeDJt3AR/Mco="}],[23,{"base64":"HzuPUtTOEVdDYWDonC6+8dcQWdPayPdaklGtJhAqatY="}],[25,{"base64":"ImOtcQFNqjqjXvTtCcHbSb0plc4ZPS5tMCatNBvNhUk="}],[26,{"base64":"UIWrHYiHSPUpuJ0cWo4h3qPrd3LK1U15I6t7mqOHjI4="}],[27,{"base64":"3E75SJdSr9U3mraK52wAuB37w9+ad6rjtN6wJxhK4bo="}],[29,{"base64":"Bw67KlSsKGfLDZFgnzjRcqaJFwom+opx3a8ji89aKGo="}],[30,{"base64":"aFq9cv4Ycux9wJnd5OkqUBgwAF3rr5eNBpH+sKq16is="}],[31,{"base64":"K431XrhGXOjNpk5WIzUqJfgMcL9uoQCQVWuUsAdkZPw="}]]}},{"Inode_extender":{"length":2,"segment":[4,1,3,1,3,1],"proof":{"base64":"Az7rN2y4RSZJyevu4cCt7L4YFfQgCaNsmIwfLIgxD80="}}},{"Value":{"base64":"y0Tw6KqK"}},{"Inode":{"length":941266,"proofs":[[6,{"base64":"gVyCoO499tM1dvvXPw9tevGMDBzhcB1lp92wFwTN7e4="}],[9,{"base64":"kMT9lhosj61kNAYvRbLp1lEo9r43Bl370tDkKBLdSk0="}],[15,{"base64":"1x5bpyZM1Dimtk7xlNy+XHgJeMiClS6OKMKAQe/BASU="}],[18,{"base64":"J9AZRApY8SNNZm5WbsxaWRaw+QSKfOWIV1S8Ojyty9A="}],[21,{"base64":"R4kpTAwl/zenGAoeictNpPJHS+mc2J5kQtEZYywctqQ="}],[22,{"base64":"/WdN4O1oVzNcnrXEOOxLrHgxWU3GunO6c8V/HnqzxpE="}],[25,{"base64":"Z4DekPCaUNKX5ixeT7XN3dQHbkPvlpK+PdTs+ybkZus="}],[28,{"base64":"x80e4aDxqbqRcZu340sLnY5sYk1tjBlu32fUCUovLrk="}],[30,{"base64":"H3C3pT3LrevmhWP48/39kFUxX3B/HLc6UHy4M9p5eTY="}]]}}]}
    let input = "0003005dff7f91229e816d2d7a156ce89abced3e8d10384de6e87d2a1be4a67bc228b401419d26f0b42c104b4e3e6473c90bf1e056be1fe2e79c6b04583db84bb92227b6000004f900000000034f49cc03000000000000000704090220406c4ced4089162da23081e1364a8d7b6679c690b9f4eae130797e3180e6ccbcef02000000000001d97b01010ffc112223360c2969070023ee2007419ac8361ecebbdd2e627bb95790badec100000110e520d8d3e6158aa0db4c45e454a29c18755cde57791ffb137157db7b98b3520001ddf5f7e41b250dd2513cc22c43363b9b9f91a9a9368a6b6b242c7b58cb4b81f201619150d4a0e534cb1002fd34a6b7dd5c2a78132919b4100ff69fd2aea3d915bb019aa9365f210b53cb4159a2244125e34fcf553c9b9c9e5ec4773e6a88878c7c500121a41be21c1a5c2dbcba4ae4a08d9b7d9ba771d2f086d2999ccd246d8373454201f67deeb2b065b07824e59e505810ae300c9f0bac90b88078af34ed72ba05c7ca011ac8ab181f97fc76c7b0a9692ea97bf318c10df7958bc87961e590db542e1b5201501e4298c829490e2234d7d58c1e6d23be65282525b390b21c4795030e1de51001746da6a93d0c2cc2c9f5960955f554fe5c8ad7cfdc0426403db39be9d7feee1b016e0e3a53b2b77699abea62955fb4e79f593c94c49e8bc77cd7218c4046328f5d01d5b92e620ffbf3f8ea693452e39994890bdf9c02bcbd06b4b137f9102d553aac012948816cfb57ae3edfb8ecc648b0c260ad5eb708a5c2255e8861a2941ce096180192cb362b243259fde98b3cde68b954ba0a257e661f3fcf30c4b2368256eb8ea001ce4fbceb8bebe88f0e56095f47c5f1b4435674015185521f5c82f948ea6cfff401acafd88d2341754520591853b72054dfe48e88c69aa0dab36ea5bf6e0c91f3b101f1c0d61d1842cc6a061e6630cd5163ba61b02eb4b9317c08caadd9a365bdb3850001838c4af39ced9ae995f0c4a160f3700c3706faf0957ab99e78326ddc047f31ca00011f3b8f52d4ce1157436160e89c2ebef1d71059d3dac8f75a9251ad26102a6ad600012263ad71014daa3aa35ef4ed09c1db49bd2995ce193d2e6d3026ad341bcd8549015085ab1d888748f529b89d1c5a8e21dea3eb7772cad54d7923ab7b9aa3878c8e01dc4ef9489752afd5379ab68ae76c00b81dfbc3df9a77aae3b4deb027184ae1ba0001070ebb2a54ac2867cb0d91609f38d172a689170a26fa8a71ddaf238bcf5a286a01685abd72fe1872ec7dc099dde4e92a501830005debaf978d0691feb0aab5ea2b012b8df55eb8465ce8cda64e5623352a25f80c70bf6ea10090556b94b0076464fc0300000000000000020420461186033eeb376cb8452649c9ebeee1c0adecbe1815f42009a36c988c1f2c88310fcd0000000006cb44f0e8aa8a0200000000000e5cd200000001320601815c82a0ee3df6d33576fbd73f0f6d7af18c0c1ce1701d65a7ddb01704cdedee090190c4fd961a2c8fad6434062f45b2e9d65128f6be37065dfbd2d0e42812dd4a4d0f01d71e5ba7264cd438a6b64ef194dcbe5c780978c882952e8e28c28041efc10125120127d019440a58f1234d666e566ecc5a5916b0f9048a7ce5885754bc3a3cadcbd015014789294c0c25ff37a7180a1e89cb4da4f2474be99cd89e6442d119632c1cb6a41601fd674de0ed6857335c9eb5c438ec4bac7831594dc6ba73ba73c57f1e7ab3c69119016780de90f09a50d297e62c5e4fb5cdddd4076e43ef9692be3dd4ecfb26e466eb1c01c7cd1ee1a0f1a9ba91719bb7e34b0b9d8e6c624d6d8c196edf67d4094a2f2eb91e011f70b7a53dcbadebe68563f8f3fdfd9055315f707f1cb73a507cb833da797936";
    let input = input.to_string();

    let bytes = string_to_bytes(input);

    let proof = StreamProof::of_bin(&bytes, &mut 0);
    println!("proofs: {}", proof);

    let bytes0 = StreamProof::to_bin(proof.clone());
    println!("bytes: {:?}", Bytes::new(bytes0.clone()));

    let proof0 = StreamProof::of_bin(&bytes0, &mut 0);
    println!("proofs: {}", proof0);

    assert!(proof == proof0);
}

#[test]
fn v1_tree32_tree_test() {
    //{"version":1,"before":{"Node":{"base64":"41q4A5rYhDwr/2HjPiQ2KJd5cwey+MLfdkxV7g3+hww="}},"after":{"Node":{"base64":"DLTkljKAEZo5QJQsnTN6ofm+5+rgilycCvCZ2AkyvvI="}},"state":{"Extender":{"length":10,"segment":[1,0],"proof":{"Inode_extender":{"length":7,"segment":[0,0,2],"proof":{"Inode_values":[["nf]cC",{"Node":[["[Sc|sK[",{"Inode":{"length":456077,"proofs":[[0,{"Blinded_inode":{"base64":"HOcVfbRv0pHy6nj9Q0xiV7Cl8UiE8pJJaAzlAuM2ayg="}}],[1,{"Blinded_inode":{"base64":"2usbpsAkkDoMSd/au/+ZyjBzG1enuYTtam4MRTSkpzs="}}],[3,{"Blinded_inode":{"base64":"pzfAZ4pUMuxoN9BwrYTdgZmmDo9QRK6RMzaVnazXarY="}}],[5,{"Blinded_inode":{"base64":"aYm385het3waojymDmjgpdPPfnkmMTUNMJR7mgNsGe8="}}],[7,{"Blinded_inode":{"base64":"SnZi7j/2Y3iVGJfcyTiCttrB9SBuSaU4tsggmmwUBqY="}}],[9,{"Blinded_inode":{"base64":"YLXh152vjQlW+ifUo6viIfMNAWbRyOI40gvRQwLDies="}}],[11,{"Blinded_inode":{"base64":"b/01SfFBQjKEi52AW5p7SCKS99kgAk1gz9Xdh1Kob2I="}}],[12,{"Blinded_inode":{"base64":"fWWnqCafI+r6ZYKL7FZ9SofAXLNqBCec4moeOErAeYA="}}],[13,{"Blinded_inode":{"base64":"3WM638Y66NkcD0aDJEAQSTQWFQeVGgQw0XcxVWkQ3qQ="}}],[15,{"Blinded_inode":{"base64":"An/Ww1Y15fPYATbR5ve0VcjvCutsu10rEmikWQ1DE+I="}}],[16,{"Blinded_inode":{"base64":"tdlMcMQ0hEn5QY9wkYRS7wwY3mOUJoO0ik0fLZLQucM="}}],[18,{"Blinded_inode":{"base64":"QtBMc81FoQ8INiJ75ze1g+yIatNBbLbNWLINuF1GMKI="}}],[19,{"Blinded_inode":{"base64":"ws0a/Ygk2YFt7p3IqWWUYFPaERGGWN/HB4qlGTcyaLA="}}],[20,{"Blinded_inode":{"base64":"kP44Q91Xv4jsbqdN4KRtIKkAlkXBnK1bPA5mSkj3kOQ="}}],[23,{"Blinded_inode":{"base64":"muXSpghZMRFb2JPJPSB3SovtXV7tGeFzvUtOwkL7v4Y="}}],[25,{"Blinded_inode":{"base64":"2gi/H5SfMY7xrDmLLJMrrzASz8wU2TYllKWjOEK+vjw="}}],[27,{"Blinded_inode":{"base64":"Fju5FeInyQJjgTq6HG0guD2omQSar/bYB2EDlkMY4q4="}}],[29,{"Blinded_inode":{"base64":"amLjIUHCNJiSglq1L2vQNSd6eXNkrHxhcZ1mzDl79lQ="}}],[30,{"Blinded_inode":{"base64":"wbFyLy0oXnH3q1edyCzlLnfiOZidtoPlntAzPKH6JYQ="}}],[31,{"Blinded_inode":{"base64":"IleESn3qoprMpf+n+xxm0LntJrLuBpalrE1N78OqPZY="}}]]}}],["Y/){",{"Value":{"base64":"2Oe3hoEB"}}],["MP6TW",{"Node":[["(B)",{"Value":{"base64":"OrN2Tw=="}}],["=Mj@cC/j",{"Value":{"base64":"3uNXzILmbddN"}}],["L-'ww{_^A",{"Blinded_node":{"base64":"1xp2IB+egGcqv9B+ZSdIgD50l0+TS8ByA+Vr7S7Gias="}}]]}]]}]]}}}}}}
    let input = "000101e35ab8039ad8843c2bff61e33e24362897797307b2f8c2df764c55ee0dfe870c010cb4e4963280119a3940942c9d337aa1f9bee7eae08a5c9c0af099d80932bef205000000000000000a0208200300000000000000070200050100000327056e665d6343020000031c075b53637c734b5b04000000000006f58d01001ce7157db46fd291f2ea78fd434c6257b0a5f14884f29249680ce502e3366b2800daeb1ba6c024903a0c49dfdabbff99ca30731b57a7b984ed6a6e0c4534a4a73b0400a737c0678a5432ec6837d070ad84dd8199a60e8f5044ae913336959dacd76ab604006989b7f3985eb77c1aa23ca60e68e0a5d3cf7e792631350d30947b9a036c19ef04004a7662ee3ff66378951897dcc93882b6dac1f5206e49a538b6c8209a6c1406a6040060b5e1d79daf8d0956fa27d4a3abe221f30d0166d1c8e238d20bd14302c389eb04006ffd3549f1414232848b9d805b9a7b482292f7d920024d60cfd5dd8752a86f62007d65a7a8269f23eafa65828bec567d4a87c05cb36a04279ce26a1e384ac0798000dd633adfc63ae8d91c0f46832440104934161507951a0430d17731556910dea40400027fd6c35635e5f3d80136d1e6f7b455c8ef0aeb6cbb5d2b1268a4590d4313e200b5d94c70c4348449f9418f70918452ef0c18de63942683b48a4d1f2d92d0b9c3040042d04c73cd45a10f0836227be737b583ec886ad3416cb6cd58b20db85d4630a200c2cd1afd8824d9816dee9dc8a965946053da11118658dfc7078aa519373268b00090fe3843dd57bf88ec6ea74de0a46d20a9009645c19cad5b3c0e664a48f790e40404009ae5d2a6085931115bd893c93d20774a8bed5d5eed19e173bd4b4ec242fbbf860400da08bf1f949f318ef1ac398b2c932baf3012cfcc14d9362594a5a33842bebe3c0400163bb915e227c90263813aba1c6d20b83da899049aaff6d8076103964318e2ae04006a62e32141c2349892825ab52f6bd035277a797364ac7c61719d66cc397bf65400c1b1722f2d285e71f7ab579dc82ce52e77e239989db683e59ed0333ca1fa2584002257844a7deaa29acca5ffa7fb1c66d0b9ed26b2ee0696a5ac4d4defc3aa3d9604592f297b0000000006d8e7b7868101054d50365457020000004f0328422900000000043ab3764f083d4d6a4063432f6a0000000009dee357cc82e66dd74d094c2d2777777b5f5e4103d71a76201f9e80672abfd07e652748803e74974f934bc07203e56bed2ec689ab";
    let input = input.to_string();

    let bytes = string_to_bytes(input);

    let proof = TreeProof::of_bin(&bytes, &mut 0);
    println!("proofs: {}", proof);

    let bytes0 = TreeProof::to_bin(proof.clone());
    println!("bytes: {:?}", Bytes::new(bytes0.clone()));

    let proof0 = TreeProof::of_bin(&bytes0, &mut 0);
    println!("proofs: {}", proof0);

    assert!(proof == proof0);
}
