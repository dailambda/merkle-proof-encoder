#!/usr/bin/python3

import json
import base64
import subprocess as sp

def normalize (s) :
  return json.dumps(json.loads(s.replace("\\'", "'")))

def normalize2 (s) :
  # Repr.pp prints Value as string or base64...
  p = json.loads (s)
  def f (p) :
    if isinstance(p, dict):
      for (k, v) in p.items():
        if k == "Value":
          if not isinstance(v, dict):
            # If value of "Value" is not encoded as base64, replace it with encoded one
            v = v.encode("utf-8")
            x = base64.standard_b64encode(v)
            p[k] = { "base64" : x.decode("utf-8") }
        else:
          f(v)
    elif isinstance(p, list):
      for e in p:
        f(e)
  f(p)
  return json.dumps(p)

TEZOSTESTBIN="./test_merkle_proof.exe"
TESTER=["cargo", "run"]
def test(opt):
  # Generate a byte sequence and a json as a proof
  outs = sp.run([TEZOSTESTBIN] + opt + ["gen"], encoding='utf-8', stderr=sp.PIPE)
  outs = outs.stderr.split('\n')
  repr = normalize2(outs[1])
  bytes = outs[3]

  # Encode and Decode the byte sequence given above
  outs = sp.run(TESTER + opt, input=bytes, encoding='utf-8', stdout=sp.PIPE, stderr=sp.PIPE)
  outs = outs.stdout.split("\n")
  repr1 = normalize(outs[1])
  bytes1 = outs[3]

  # Decode the byte sequence $TESTER outputs
  outs = sp.run([TEZOSTESTBIN] + opt + ["decode"], input=bytes1, encoding='utf-8', stderr=sp.PIPE)
  outs = outs.stderr.split("\n")

  repr2 = normalize2(outs[1])

  b1 = repr==repr1
  b2 = repr2==repr2
  b3 = bytes==bytes1
  if not (b1 and b2 and b3) :
    print(repr)
    print(repr1)
    print(repr2)
    print(bytes)
    print(bytes1)
    print(b1)
    print(b2)
    print(b3)
    assert False

for i in ["v1", "v2"]:
  for j in ["tree32", "tree2"]:
    for k in ["tree", "stream"]:
      opt = [i, j, k]
      print(opt)
      for l in range(100) :
        test(opt)
